EXTSYS1,MXGLCOMPInterface,,EN
ACTIVE,COMPVALUE,COMPTEXT,GLORDER,ORGID,USERID
1,128010,"Inventory Consumables",1,IP,MAXADMIN
1,128020,"Inventory Spares",1,IP,MAXADMIN
1,128030,"Inventory Refurbished Spares",1,IP,MAXADMIN
1,128035,"Inventory Refurbished Spares Prepaid",1,IP,MAXADMIN
1,145000,"Prepayments Deductible",1,IP,MAXADMIN
1,146000,"GST Receivable",1,IP,MAXADMIN
1,146900,"Error Suspense",1,IP,MAXADMIN
1,160040,"Station Property,Plant & Equip",1,IP,MAXADMIN
1,160042,"Capitalised Outages",1,IP,MAXADMIN
1,161042,"WIP Capitalised Outages",1,IP,MAXADMIN
1,161040,"WIP Station Property,Plant & Equip",1,IP,MAXADMIN
1,202000,"Accd Exp RBNI ",1,IP,MAXADMIN
1,204060,"Accd Exp FBT",1,IP,MAXADMIN
1,204100,"Payroll Clearing",1,IP,MAXADMIN
1,510000,"Coal Purchases",1,IP,MAXADMIN
1,510002,"Coal Purchase Agreement Fixed Charge",1,IP,MAXADMIN
1,510004,"Coal Purchase Agreement O&M Budget Charge",1,IP,MAXADMIN
1,510006,"Coal Purchase Agreement Equip Usage",1,IP,MAXADMIN
1,520000,"Gas Commodity Cost for Elec",1,IP,MAXADMIN
1,530100,"Electricity Network Charges",1,IP,MAXADMIN
1,530210,"Electricity Network Metering Charges",1,IP,MAXADMIN
1,600010,"Employee Salaries Base",1,IP,MAXADMIN
1,600020,"Employee Salaries Overtime",1,IP,MAXADMIN
1,600090,"Salaries Recharged",1,IP,MAXADMIN
1,600500,"External Support Labour",1,IP,MAXADMIN
1,610102,"Water Treatment",1,IP,MAXADMIN
1,610104,"Cooling Water",1,IP,MAXADMIN
1,610106,"Industrial Gases",1,IP,MAXADMIN
1,610130,"Oils",1,IP,MAXADMIN
1,610210,"Water Supply",1,IP,MAXADMIN
1,610212,"Water Supply ISA Fees",1,IP,MAXADMIN
1,610220,"Water Supply High Quality",1,IP,MAXADMIN
1,610222,"Water Supply HQ ISA Fees",1,IP,MAXADMIN
1,610300,"Ash Disposal",1,IP,MAXADMIN
1,610310,"Saline Waste Disposal",1,IP,MAXADMIN
1,610320,"Drainage",1,IP,MAXADMIN
1,610430,"Regulatory and Industry Costs",1,IP,MAXADMIN
1,610440,"ISA Fee & Corporate O'heads",1,IP,MAXADMIN
1,610450,"Equipment Usage Fee",1,IP,MAXADMIN
1,610510,"Diesel Fuel",1,IP,MAXADMIN
1,620010,"Materials",1,IP,MAXADMIN
1,620015,"Inventory Write offs",1,IP,MAXADMIN
1,620020,"General consumables",1,IP,MAXADMIN
1,620030,"Tools and Equipment",1,IP,MAXADMIN
1,620040,"Scaffolding",1,IP,MAXADMIN
1,621500,"O&M Inspection Outages Unit 1",1,IP,MAXADMIN
1,621505,"O&M Inspection Outages Unit 2",1,IP,MAXADMIN
1,630010,"Motor Vehicles Lease",1,IP,MAXADMIN
1,630011,"Motor Vehicles Insurance",1,IP,MAXADMIN
1,630012,"Motor Vehicles Maintenance",1,IP,MAXADMIN
1,630030,"Mobile Plant Maintenance",1,IP,MAXADMIN
1,630100,"Security",1,IP,MAXADMIN
1,630110,"Property Rates",1,IP,MAXADMIN
1,630200,"Cleaning and Waste Disposal",1,IP,MAXADMIN
1,630240,"Other Misc Site and Civil",1,IP,MAXADMIN
1,630400,"Business Insurance",1,IP,MAXADMIN
1,650010,"Environmental Costs",1,IP,MAXADMIN
1,650020,"EPA Fees",1,IP,MAXADMIN
1,650050,"Greenhouse Costs",1,IP,MAXADMIN
1,660010,"Audit Fees",1,IP,MAXADMIN
1,660030,"Legal Fees",1,IP,MAXADMIN
1,660040,"Engineering and Technical Consultancy",1,IP,MAXADMIN
1,660050,"Recruitment Cost",1,IP,MAXADMIN
1,660060,"Memberships and Subscriptions",1,IP,MAXADMIN
1,660080,"Research and Development",1,IP,MAXADMIN
1,660090,"Other Professional Services",1,IP,MAXADMIN
1,660220,"Hardware Purchases Non-Capital",1,IP,MAXADMIN
1,660230,"IT Contracts & Support",1,IP,MAXADMIN
1,660250,"IT Software Licences (Maintenance)",1,IP,MAXADMIN
1,660490,"IT Other",1,IP,MAXADMIN
1,660520,"Telephone Landlines",1,IP,MAXADMIN
1,662010,"Travel Domestic",1,IP,MAXADMIN
1,662020,"Travel International",1,IP,MAXADMIN
1,664010,"Office Equipment Expense",1,IP,MAXADMIN
1,664020,"General Printing and Stationary",1,IP,MAXADMIN
1,664030,"Postage and Couriers",1,IP,MAXADMIN
1,664040,"Staff Amenities",1,IP,MAXADMIN
1,664050,"Magazines and Periodicals",1,IP,MAXADMIN
1,664060,"Office Costs Misc",1,IP,MAXADMIN
1,665010,"Corporate Affairs CSR Corporate Programs",1,IP,MAXADMIN
1,665020,"Corporate Affairs CSR Regional Programs",1,IP,MAXADMIN
1,665030,"Corporate Affairs CSR Marketing and Admin",1,IP,MAXADMIN
1,667010,"Protective Equipment",1,IP,MAXADMIN
1,667020,"Health and Safety Medical",1,IP,MAXADMIN
1,669010,"Entertainment General",1,IP,MAXADMIN
1,669210,"Training and Professional Development",1,IP,MAXADMIN
1,669700,"Management Fees Expense INTERCO",1,IP,MAXADMIN
1,750010,"Bank Fees & Charges",1,IP,MAXADMIN