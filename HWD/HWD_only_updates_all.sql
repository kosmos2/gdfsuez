update ACCOUNTDEFAULTS set GLDEFAULT = replace(GLDEFAULT,'53120','621010') where GLDEFAULT like '%-53120'; -- 2 rows updated
update ORGANIZATION set CLEARINGACCT = replace(CLEARINGACCT,'52205','620090') where CLEARINGACCT like '%-52205'; -- 1 row updated
commit;
update COMPANIES set RBNIACC = replace(RBNIACC,'21120','202000') where RBNIACC like '%-21120'; --2028 --2036 --2046 records updated
update COMPANYACCDEF set RBNIACC = replace(RBNIACC,'21120','202000') where RBNIACC like '%-21120'; -- 1 row updated
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'54705','665010') where GLACCOUNT like '%-54705'; -- 1 row updated
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'55110','669210') where GLACCOUNT like '%-55110'; -- 5 rows updated
update INVCOST set CONTROLACC = replace(CONTROLACC,'11510','128010') where CONTROLACC like '%-11510'; --26276 --26399 rows updated
update INVCOST set CONTROLACC = replace(CONTROLACC,'52200','620015') where CONTROLACC like '%-52200'; -- 170 rows updated
update INVCOST set SHRINKAGEACC = replace(SHRINKAGEACC,'52200','620015') where SHRINKAGEACC like '%-52200'; -- 24691 --24763 rows updated
update INVCOST set SHRINKAGEACC = replace(SHRINKAGEACC,'52205','620090') where SHRINKAGEACC like '%-52205'; -- 1755 --1806 rows updated
update INVCOST set INVCOSTADJACC = replace(INVCOSTADJACC,'52205','620090') where INVCOSTADJACC like '%-52205'; -- 26446 --26569 rows updated
update INVENTORY set CONTROLACC = replace(CONTROLACC,'11510','128010') where CONTROLACC like '%-11510'; -- 26112 --26235 rows updated
update INVENTORY set CONTROLACC = replace(CONTROLACC,'52200','620015') where CONTROLACC like '%-52200'; -- 170 rows updated
update INVENTORY set INVCOSTADJACC = replace(INVCOSTADJACC,'52205','620090') where INVCOSTADJACC like '%-52205'; -- 26282 --26405 rows updated
update INVENTORY set SHRINKAGEACC = replace(SHRINKAGEACC,'52200','620015') where SHRINKAGEACC like '%-52200'; -- 24527 --24599 rows updated
update INVENTORY set SHRINKAGEACC = replace(SHRINKAGEACC,'52205','620090') where SHRINKAGEACC like '%-52205'; -- 1755 --1806 rows updated
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'52100','620010') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-52100'; -- 12 -2 rows
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'53170','621050') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-53170'; -- 3 --1 rows
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'54440','620030') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-54440'; -- 3 0 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'54570','664060') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-54570'; -- 2 0 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'54810','667030') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-54810'; -- 1 0 rows
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'54990','620010') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-54990'; -- 1 0 row
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'55320','660040') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-55320'; -- lastrun 1 row
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'56170','610130') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-56170'; -- 8 --4 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'57320','630310') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-57320'; -- 1 row
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'11510','128010') where ponum is not null and ponum in (select ponum from PO where historyflag=0) AND GLACCOUNT like '%-11510'; --79 --120 rows
update INVUSELINE set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where GLDEBITACCT like '%-11510'; -- 188 --192 rows updated
update INVUSELINE set GLDEBITACCT = replace(GLDEBITACCT,'52200','620015') where GLDEBITACCT like '%-52200'; -- 1 row updated
update INVUSELINE set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510'; -- 188 --192 rows updated
update INVUSELINE set GLCREDITACCT = replace(GLCREDITACCT,'52200','620015') where GLCREDITACCT like '%-52200'; -- 1 row updated
update LABORCRAFTRATE set GLACCOUNT = replace(GLACCOUNT,'53120','621010') where GLACCOUNT like '%-53120'; -- 5939 --5945 rows updated
commit;
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11130','100100') where GLACCOUNT like '%-11130';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11131','109000') where GLACCOUNT like '%-11131';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11133','109000') where GLACCOUNT like '%-11133';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11420','124303') where GLACCOUNT like '%-11420';--1 row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11455','128025') where GLACCOUNT like '%-11455';--1 row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11460','124100') where GLACCOUNT like '%-11460';--1 row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11505','128050') where GLACCOUNT like '%-11505';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11510','128010') where GLACCOUNT like '%-11510';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11512','128040') where GLACCOUNT like '%-11512';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'11910','145000') where GLACCOUNT like '%-11910';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'15590','192900') where GLACCOUNT like '%-15590';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'15710','172300') where GLACCOUNT like '%-15710';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'15720','172300') where GLACCOUNT like '%-15720';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'15791','172300') where GLACCOUNT like '%-15791';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21120','202000') where GLACCOUNT like '%-21120';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21150','202020') where GLACCOUNT like '%-21150';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21152','202150') where GLACCOUNT like '%-21152';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21230','204060') where GLACCOUNT like '%-21230';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21261','146000') where GLACCOUNT like '%-21261';--2 rows
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21410','220100') where GLACCOUNT like '%-21410';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21420','220110') where GLACCOUNT like '%-21420';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21440','220400') where GLACCOUNT like '%-21440';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'21610','242800') where GLACCOUNT like '%-21610';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'25460','270120') where GLACCOUNT like '%-25460';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'41100','400100') where GLACCOUNT like '%-41100';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'41200','500600') where GLACCOUNT like '%-41200';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'41505','540100') where GLACCOUNT like '%-41505';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51110','600010') where GLACCOUNT like '%-51110';--5 row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51120','600020') where GLACCOUNT like '%-51120';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51130','600190') where GLACCOUNT like '%-51130';--5 row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51210','600180') where GLACCOUNT like '%-51210';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51220','600140') where GLACCOUNT like '%-51220';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51221','600150') where GLACCOUNT like '%-51221';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51230','600160') where GLACCOUNT like '%-51230';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51240','600290') where GLACCOUNT like '%-51240';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51250','600110') where GLACCOUNT like '%-51250';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51260','600120') where GLACCOUNT like '%-51260';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51270','600192') where GLACCOUNT like '%-51270';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51280','600290') where GLACCOUNT like '%-51280';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51290','760100') where GLACCOUNT like '%-51290';--1row
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51320','600040') where GLACCOUNT like '%-51320';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51350','600050') where GLACCOUNT like '%-51350';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51390','600210') where GLACCOUNT like '%-51390';-- 48 rows
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51410','892100') where GLACCOUNT like '%-51410';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51510','600500') where GLACCOUNT like '%-51510';-- 57 rows
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51520','600010') where GLACCOUNT like '%-51520';-- 26 rows
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51525','600210') where GLACCOUNT like '%-51525';--9
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'52100','620010') where GLACCOUNT like '%-52100';--65
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'52200','620015') where GLACCOUNT like '%-52200';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'52205','620090') where GLACCOUNT like '%-52205';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'53100','621000') where GLACCOUNT like '%-53100';--68
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'53120','621010') where GLACCOUNT like '%-53120';--49
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'53160','621020') where GLACCOUNT like '%-53160';--7
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'53170','621050') where GLACCOUNT like '%-53170';--7
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54100','669500') where GLACCOUNT like '%-54100';--63
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54130','669700') where GLACCOUNT like '%-54130';--27
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54135','669700') where GLACCOUNT like '%-54135';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54140','669700') where GLACCOUNT like '%-54140';--27
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54150','660060') where GLACCOUNT like '%-54150';--42
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54210','530100') where GLACCOUNT like '%-54210' AND GLACCOUNT NOT like '6%';--2--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54210','610020') where GLACCOUNT like '%-54210' AND GLACCOUNT like '6%';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54220','610410') where GLACCOUNT like '%-54220';--13
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54310','662020') where GLACCOUNT like '%-54310';--47
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54320','662010') where GLACCOUNT like '%-54320';--50
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54330','669010') where GLACCOUNT like '%-54330';--48
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54340','664040') where GLACCOUNT like '%-54340';--48
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54360','662110') where GLACCOUNT like '%-54360';--48
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54440','620030') where GLACCOUNT like '%-54440';--54
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54450','620010') where GLACCOUNT like '%-54450';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54540','630900') where GLACCOUNT like '%-54540';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54560','660210') where GLACCOUNT like '%-54560';--40
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54570','664060') where GLACCOUNT like '%-54570';--50
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54580','630180') where GLACCOUNT like '%-54580';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54610','660060') where GLACCOUNT like '%-54610';--7
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54650','660030') where GLACCOUNT like '%-54650';--25
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54660','660010') where GLACCOUNT like '%-54660';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54670','660025') where GLACCOUNT like '%-54670';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54680','660040') where GLACCOUNT like '%-54680';--48
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54685','660090') where GLACCOUNT like '%-54685';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54705','665010') where GLACCOUNT like '%-54705';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54710','665010') where GLACCOUNT like '%-54710';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54720','669050') where GLACCOUNT like '%-54720';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54725','665010') where GLACCOUNT like '%-54725';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54740','660050') where GLACCOUNT like '%-54740';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54750','600500') where GLACCOUNT like '%-54750';--8
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54751','600500') where GLACCOUNT like '%-54751';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54752','600500') where GLACCOUNT like '%-54752';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54753','665010') where GLACCOUNT like '%-54753';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54760','660250') where GLACCOUNT like '%-54760';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54770','660260') where GLACCOUNT like '%-54770';--15
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54780','660510') where GLACCOUNT like '%-54780';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54790','669500') where GLACCOUNT like '%-54790';--20
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54810','667030') where GLACCOUNT like '%-54810';--52
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54820','667010') where GLACCOUNT like '%-54820';--53
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54850','630440') where GLACCOUNT like '%-54850';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54920','640510') where GLACCOUNT like '%-54920';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54925','750010') where GLACCOUNT like '%-54925';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54950','894000') where GLACCOUNT like '%-54950';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54951','894002') where GLACCOUNT like '%-54951';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54952','894002') where GLACCOUNT like '%-54952';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54960','664060') where GLACCOUNT like '%-54960';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'54990','620010') where GLACCOUNT like '%-54990';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55110','669210') where GLACCOUNT like '%-55110';--45
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55210','621000') where GLACCOUNT like '%-55210';--20
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55310','660080') where GLACCOUNT like '%-55310';--17
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55320','660040') where GLACCOUNT like '%-55320';--26
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55330','620100') where GLACCOUNT like '%-55330';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'55340','620120') where GLACCOUNT like '%-55340';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56111','610108') where GLACCOUNT like '%-56111';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56112','610110') where GLACCOUNT like '%-56112';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56113','610112') where GLACCOUNT like '%-56113';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56114','610114') where GLACCOUNT like '%-56114';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56120','610116') where GLACCOUNT like '%-56120';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56131','610118') where GLACCOUNT like '%-56131';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56132','610120') where GLACCOUNT like '%-56132';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56133','610122') where GLACCOUNT like '%-56133';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56134','610124') where GLACCOUNT like '%-56134';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56140','610102') where GLACCOUNT like '%-56140';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56151','520000') where GLACCOUNT like '%-56151';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56152','610128') where GLACCOUNT like '%-56152';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56160','610180') where GLACCOUNT like '%-56160';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56170','610130') where GLACCOUNT like '%-56170';--10
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56210','610220') where GLACCOUNT like '%-56210';--15
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56310','610310') where GLACCOUNT like '%-56310';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56410','630180') where GLACCOUNT like '%-56410';--6
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56510','610510') where GLACCOUNT like '%-56510';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56520','510100') where GLACCOUNT like '%-56520';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56610','610010') where GLACCOUNT like '%-56610';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56710','620010') where GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '6%' OR GLACCOUNT LIKE '7%');--12
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'56710','620120') where GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '8%');--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57110','630010') where GLACCOUNT like '%-57110';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57120','630020') where GLACCOUNT like '%-57120';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57130','630040') where GLACCOUNT like '%-57130';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57140','630020') where GLACCOUNT like '%-57140';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57150','630090') where GLACCOUNT like '%-57150';--11
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57210','630100') where GLACCOUNT like '%-57210';--16
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57310','630300') where GLACCOUNT like '%-57310';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57320','630310') where GLACCOUNT like '%-57320';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57410','630230') where GLACCOUNT like '%-57410';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57415','630220') where GLACCOUNT like '%-57415';--6
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57420','630200') where GLACCOUNT like '%-57420';--3
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57430','630210') where GLACCOUNT like '%-57430';--25
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57520','630200') where GLACCOUNT like '%-57520';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57530','630200') where GLACCOUNT like '%-57530';--4
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57610','650010') where GLACCOUNT like '%-57610';--12
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57620','650020') where GLACCOUNT like '%-57620';--11
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57630','650030') where GLACCOUNT like '%-57630';--7
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57640','650040') where GLACCOUNT like '%-57640';--7
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57650','650040') where GLACCOUNT like '%-57650';--5
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57660','650050') where GLACCOUNT like '%-57660';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57710','630150') where GLACCOUNT like '%-57710';--1--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57720','630140') where GLACCOUNT like '%-57720';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'57810','630110') where GLACCOUNT like '%-57810';--1--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'58100','540300') where GLACCOUNT like '%-58100';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'58200','540300') where GLACCOUNT like '%-58200';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'60100','669900') where GLACCOUNT like '%-60100';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'70100','690040') where GLACCOUNT like '%-70100';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'70200','690032') where GLACCOUNT like '%-70200';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'70400','692300') where GLACCOUNT like '%-70400';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'70600','690046') where GLACCOUNT like '%-70600';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'80120','750041') where GLACCOUNT like '%-80120';--1
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'80130','750010') where GLACCOUNT like '%-80130';--2
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'80140','750300') where GLACCOUNT like '%-80140';--1
commit;
update LOCATIONS set OLDCONTROLACC = replace(OLDCONTROLACC,'11510','128010') where OLDCONTROLACC like '%-11510';--7 rows updated
update LOCATIONS set OLDCONTROLACC = replace(OLDCONTROLACC,'52200','620015') where OLDCONTROLACC like '%-52200';--2 rows updated
update LOCATIONS set PURCHVARACC = replace(PURCHVARACC,'52205','620090') where PURCHVARACC like '%-52205';-- 9 rows updated
update LOCATIONS set CONTROLACC = replace(CONTROLACC,'11510','128010') where CONTROLACC like '%-11510'; -- 7 rows updated
update LOCATIONS set CONTROLACC = replace(CONTROLACC,'52200','620015') where CONTROLACC like '%-52200';--2 rows updated
update LOCATIONS set CURVARACC = replace(CURVARACC,'52205','620090') where CURVARACC like '%-52205'; --9 rows updated
update LOCATIONS set SHRINKAGEACC = replace(SHRINKAGEACC,'52200','620015') where SHRINKAGEACC like '%-52200'; -- 5 rows updated
update LOCATIONS set SHRINKAGEACC = replace(SHRINKAGEACC,'52205','620090') where SHRINKAGEACC like '%-52205'; -- 4 rows updated
update LOCATIONS set RECEIPTVARACC = replace(RECEIPTVARACC,'52205','620090') where RECEIPTVARACC like '%-52205'; -- 9 rows updated
update LOCATIONS set INVOICEVARACC = replace(INVOICEVARACC,'52205','620090') where INVOICEVARACC like '%-52205';-- 9 rows updated
update LOCATIONS set OLDSHRINKAGEACC = replace(OLDSHRINKAGEACC,'52200','620015') where OLDSHRINKAGEACC like '%-52200'; -- 5 rows updated
update LOCATIONS set OLDSHRINKAGEACC = replace(OLDSHRINKAGEACC,'52205','620090') where OLDSHRINKAGEACC like '%-52205'; -- 4 rows updated
update LOCATIONS set INVCOSTADJACC = replace(INVCOSTADJACC,'52205','620090') where INVCOSTADJACC like '%-52205';-- 9 rows updated
update LOCATIONS set OLDINVCOSTADJACC = replace(OLDINVCOSTADJACC,'52205','620090') where OLDINVCOSTADJACC like '%-52205';-- 9 rows updated
update TAX set EXCLUSIVEGL = replace(EXCLUSIVEGL,'21261','146000') where EXCLUSIVEGL like '%-21261'; -- 2 rows updated
update TAX set INCLUSIVEGL = replace(INCLUSIVEGL,'21261','146000') where INCLUSIVEGL like '%-21261'; -- 2 rows updated
update CHARTOFACCOUNTS set GLACCOUNT = replace(GLACCOUNT,'52205','620090') where GLACCOUNT like '%-52205';--1
update CHARTOFACCOUNTS set GLACCOUNT = replace(GLACCOUNT,'54610','660060') where GLACCOUNT like '%-54610'; -- 2 rows
update CHARTOFACCOUNTS set GLACCOUNT = replace(GLACCOUNT,'57120','630020') where GLACCOUNT like '%-57120'; --1 row
update CHARTOFACCOUNTS set GLACCOUNT = replace(GLACCOUNT,'80130','750010') where GLACCOUNT like '%-80130'; -- 1 row
update CHARTOFACCOUNTS set GLCOMP03 = replace(GLCOMP03,'52205','620090') where GLCOMP03 = '52205'; -- 1 row
update CHARTOFACCOUNTS set GLCOMP03 = replace(GLCOMP03,'54610','660060') where GLCOMP03 = '54610'; -- 2 rows
update CHARTOFACCOUNTS set GLCOMP03 = replace(GLCOMP03,'57120','630020') where GLCOMP03 = '57120'; -- 1 row
update CHARTOFACCOUNTS set GLCOMP03 = replace(GLCOMP03,'80130','750010') where GLCOMP03 = '80130'; -- 1 row
commit;
update ASSETTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where GLDEBITACCT like '%-11510' and transdate > '31/DEC/2014';-- 36 rows updated--71 rows updated.
update ASSETTRANS set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510' and transdate > '31/DEC/2014';-- 22 rows updated--41 rows updated.
update INVOICETRANS set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where GLCREDITACCT like '%-21120' and transdate > '31/DEC/2014';--8671 rows updated--10,765 rows updated.
update INVOICETRANS set GLDEBITACCT = replace(GLDEBITACCT,'21120','202000') where GLDEBITACCT like '%-21120' and transdate > '31/DEC/2014';--6532 rows updated--8,092 rows updated.
update INVOICETRANS set GLDEBITACCT = replace(GLDEBITACCT,'21261','146000') where GLDEBITACCT like '%-21261' and transdate > '31/DEC/2014';--8656 rows updated--10,749 rows updated.
update INVOICETRANS set GLDEBITACCT = replace(GLDEBITACCT,'52205','620090') where GLDEBITACCT like '%-52205' and transdate > '31/DEC/2014';--15 rows updated--16 rows updated.
update INVTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where GLDEBITACCT like '%-11510' and transdate > '31/DEC/2014';--12174 rows updated--12,833 rows updated.
update INVTRANS set GLDEBITACCT = replace(GLDEBITACCT,'52200','620015') where GLDEBITACCT like '%-52200' and transdate > '31/DEC/2014';--2875 rows updated--3,216 rows updated.
update INVTRANS set GLDEBITACCT = replace(GLDEBITACCT,'52205','620090') where GLDEBITACCT like '%-52205' and transdate > '31/DEC/2014';--386 rows updated--505 rows updated.
update INVTRANS set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510' and transdate > '31/DEC/2014';--150 rows updated--273 rows updated.
update INVTRANS set GLCREDITACCT = replace(GLCREDITACCT,'52200','620015') where GLCREDITACCT like '%-52200' and transdate > '31/DEC/2014';--14257 rows updated--15,016 rows updated.
update INVTRANS set GLCREDITACCT = replace(GLCREDITACCT,'52205','620090') where GLCREDITACCT like '%-52205' and transdate > '31/DEC/2014';--1028 rows updated--1,265 rows updated.
update LABTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where GLDEBITACCT like '%-53120' and transdate > '31/DEC/2014';--21346 rows updated--27,409 rows updated.
update MATRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510' and transdate > '31/DEC/2014';--1093 rows updated--1,480 rows updated.
update MATRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'11512','128040') where GLCREDITACCT like '%-11512' and transdate > '31/DEC/2014';--156 rows updated--216 rows updated.
update MATRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where GLCREDITACCT like '%-21120' and transdate > '31/DEC/2014';--3611 rows updated--4,547 rows updated.
update MATRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'52205','620090') where GLCREDITACCT like '%-52205' and transdate > '31/DEC/2014';-- 32 rows updated--34 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11460','124100') where GLDEBITACCT like '%-11460' and transdate > '31/DEC/2014';--2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where GLDEBITACCT like '%-11510' and transdate > '31/DEC/2014';--1924 rows updated--2,503 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11512','128040') where GLDEBITACCT like '%-11512' and transdate > '31/DEC/2014';--183 rows updated--244 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where GLDEBITACCT like '%-52100' and transdate > '31/DEC/2014';--1700--2,194 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'52205','620090') where GLDEBITACCT like '%-52205' and transdate > '31/DEC/2014';--39--41 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where GLDEBITACCT like '%-53100' and transdate > '31/DEC/2014';--57--58 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where GLDEBITACCT like '%-53170' and transdate > '31/DEC/2014';--287--384 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where GLDEBITACCT like '%-54100' and transdate > '31/DEC/2014';--2--2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where GLDEBITACCT like '%-54440' and transdate > '31/DEC/2014';--163--197 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where GLDEBITACCT like '%-54560' and transdate > '31/DEC/2014';--2--2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where GLDEBITACCT like '%-54570' and transdate > '31/DEC/2014';--67--91 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where GLDEBITACCT like '%-54580' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where GLDEBITACCT like '%-54610' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where GLDEBITACCT like '%-54705' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where GLDEBITACCT like '%-54760' and transdate > '31/DEC/2014';--8--11 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where GLDEBITACCT like '%-54780' and transdate > '31/DEC/2014';--30--39 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where GLDEBITACCT like '%-54810' and transdate > '31/DEC/2014';--4--5 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where GLDEBITACCT like '%-54820' and transdate > '31/DEC/2014';--8--12 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54990','620010') where GLDEBITACCT like '%-54990' and transdate > '31/DEC/2014';--4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where GLDEBITACCT like '%-55110' and transdate > '31/DEC/2014';--5--5 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where GLDEBITACCT like '%-55320' and transdate > '31/DEC/2014';--2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56111','610108') where GLDEBITACCT like '%-56111' and transdate > '31/DEC/2014';--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56112','610110') where GLDEBITACCT like '%-56112' and transdate > '31/DEC/2014';--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where GLDEBITACCT like '%-56131' and transdate > '31/DEC/2014';--9--11 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56132','610120') where GLDEBITACCT like '%-56132' and transdate > '31/DEC/2014';--32--38 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56133','610122') where GLDEBITACCT like '%-56133' and transdate > '31/DEC/2014';--44--54 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where GLDEBITACCT like '%-56134' and transdate > '31/DEC/2014';--4--4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56140','610102') where GLDEBITACCT like '%-56140' and transdate > '31/DEC/2014';--12--14 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where GLDEBITACCT like '%-56170' and transdate > '31/DEC/2014';--47--52 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56210','610220') where GLDEBITACCT like '%-56210' and transdate > '31/DEC/2014';--20--20 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--56--62 
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where GLDEBITACCT like '%-57210' and transdate > '31/DEC/2014';--20--20 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where GLDEBITACCT like '%-57310' and transdate > '31/DEC/2014';--10--11 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where GLDEBITACCT like '%-57320' and transdate > '31/DEC/2014';--32--35 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where GLDEBITACCT like '%-57410' and transdate > '31/DEC/2014';--1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where GLDEBITACCT like '%-57430' and transdate > '31/DEC/2014';--18--19 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where GLDEBITACCT like '%-60100' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATUSETRANS set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510' and transdate > '31/DEC/2014';--1607 rows--2,115 rows updated.
update MATUSETRANS set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where GLCREDITACCT like '%-21120' and transdate > '31/DEC/2014';--2746--3,489 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'11460','124100') where GLDEBITACCT like '%-11460' and transdate > '31/DEC/2014';--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where GLDEBITACCT like '%-52100' and transdate > '31/DEC/2014';--3195--4,165 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where GLDEBITACCT like '%-53100' and transdate > '31/DEC/2014';--59--60 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where GLDEBITACCT like '%-53170' and transdate > '31/DEC/2014';--308--411 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where GLDEBITACCT like '%-54100' and transdate > '31/DEC/2014';--2--2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where GLDEBITACCT like '%-54440' and transdate > '31/DEC/2014';--187--226 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where GLDEBITACCT like '%-54560' and transdate > '31/DEC/2014';--2--2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where GLDEBITACCT like '%-54570' and transdate > '31/DEC/2014';--69--94 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where GLDEBITACCT like '%-54580' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where GLDEBITACCT like '%-54610' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where GLDEBITACCT like '%-54705' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where GLDEBITACCT like '%-54760' and transdate > '31/DEC/2014';--8--11 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where GLDEBITACCT like '%-54780' and transdate > '31/DEC/2014';--30--39 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where GLDEBITACCT like '%-54810' and transdate > '31/DEC/2014';--6--8 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where GLDEBITACCT like '%-54820' and transdate > '31/DEC/2014';--9--14 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'54990','620010') where GLDEBITACCT like '%-54990' and transdate > '31/DEC/2014';--4--9 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where GLDEBITACCT like '%-55110' and transdate > '31/DEC/2014';--5--5 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where GLDEBITACCT like '%-55320' and transdate > '31/DEC/2014';--3 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56111','610108') where GLDEBITACCT like '%-56111' and transdate > '31/DEC/2014';--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56112','610110') where GLDEBITACCT like '%-56112' and transdate > '31/DEC/2014';--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56113','610112') where GLDEBITACCT like '%-56113' and transdate > '31/DEC/2014';--1--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where GLDEBITACCT like '%-56131' and transdate > '31/DEC/2014';--9--11 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56132','610120') where GLDEBITACCT like '%-56132' and transdate > '31/DEC/2014';--32--38 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56133','610122') where GLDEBITACCT like '%-56133' and transdate > '31/DEC/2014';--44--54 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where GLDEBITACCT like '%-56134' and transdate > '31/DEC/2014';--4--4 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56140','610102') where GLDEBITACCT like '%-56140' and transdate > '31/DEC/2014';--12--14 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where GLDEBITACCT like '%-56170' and transdate > '31/DEC/2014';--50--55 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56210','610220') where GLDEBITACCT like '%-56210' and transdate > '31/DEC/2014';--20--20 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--58--65
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where GLDEBITACCT like '%-57210' and transdate > '31/DEC/2014';--21--21 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where GLDEBITACCT like '%-57310' and transdate > '31/DEC/2014';--12--13 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where GLDEBITACCT like '%-57320' and transdate > '31/DEC/2014';--35--38 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where GLDEBITACCT like '%-57410' and transdate > '31/DEC/2014';--1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where GLDEBITACCT like '%-57430' and transdate > '31/DEC/2014';--18--19 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where GLDEBITACCT like '%-60100' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where GLCREDITACCT like '%-21120' and transdate > '31/DEC/2014';--6221 rows--7,975 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'11910','145000') where GLDEBITACCT like '%-11910' and transdate > '31/DEC/2014';--1--3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'21261','146000') where GLDEBITACCT like '%-21261' and transdate > '31/DEC/2014';--4--5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'21610','242800') where GLDEBITACCT like '%-21610' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'41200','500600') where GLDEBITACCT like '%-41200' and transdate > '31/DEC/2014';--10--12 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'41505','540100') where GLDEBITACCT like '%-41505' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51220','600140') where GLDEBITACCT like '%-51220' and transdate > '31/DEC/2014';--8--10 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51390','600210') where GLDEBITACCT like '%-51390' and transdate > '31/DEC/2014';--57--68 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where GLDEBITACCT like '%-51510' and transdate > '31/DEC/2014';--115--152 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51520','600010') where GLDEBITACCT like '%-51520' and transdate > '31/DEC/2014';--5--8 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51525','600210') where GLDEBITACCT like '%-51525' and transdate > '31/DEC/2014';--2--3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where GLDEBITACCT like '%-52100' and transdate > '31/DEC/2014';--73--94 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where GLDEBITACCT like '%-53100' and transdate > '31/DEC/2014';--1886--2,466 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where GLDEBITACCT like '%-53120' and transdate > '31/DEC/2014';--959--1,263 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53160','621020') where GLDEBITACCT like '%-53160' and transdate > '31/DEC/2014';--10--15 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where GLDEBITACCT like '%-53170' and transdate > '31/DEC/2014';--158--252 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where GLDEBITACCT like '%-54100' and transdate > '31/DEC/2014';--16--18 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54130','669700') where GLDEBITACCT like '%-54130' and transdate > '31/DEC/2014';--5--7 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54135','669700') where GLDEBITACCT like '%-54135' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54140','669700') where GLDEBITACCT like '%-54140' and transdate > '31/DEC/2014';--9--11 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54150','660060') where GLDEBITACCT like '%-54150' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54210','530100') where GLDEBITACCT like '%-54210' and transdate > '31/DEC/2014' AND GLDEBITACCT NOT like '6%';--2--3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54210','610020') where GLDEBITACCT like '%-54210' and transdate > '31/DEC/2014' AND GLDEBITACCT like '6%';--4--5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54220','610410') where GLDEBITACCT like '%-54220' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54310','662020') where GLDEBITACCT like '%-54310' and transdate > '31/DEC/2014';--4--4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54320','662010') where GLDEBITACCT like '%-54320' and transdate > '31/DEC/2014';--60--78 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54330','669010') where GLDEBITACCT like '%-54330' and transdate > '31/DEC/2014';--23--26 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where GLDEBITACCT like '%-54340' and transdate > '31/DEC/2014';--17--17 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54360','662110') where GLDEBITACCT like '%-54360' and transdate > '31/DEC/2014';--2--2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where GLDEBITACCT like '%-54440' and transdate > '31/DEC/2014';--49--55 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where GLDEBITACCT like '%-54560' and transdate > '31/DEC/2014';--8--8 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where GLDEBITACCT like '%-54570' and transdate > '31/DEC/2014';--362--430 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where GLDEBITACCT like '%-54580' and transdate > '31/DEC/2014';--13--14 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where GLDEBITACCT like '%-54610' and transdate > '31/DEC/2014';--3--4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where GLDEBITACCT like '%-54650' and transdate > '31/DEC/2014';--42--57 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54660','660010') where GLDEBITACCT like '%-54660' and transdate > '31/DEC/2014';--1--2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where GLDEBITACCT like '%-54670' and transdate > '31/DEC/2014';--1--2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54680','660040') where GLDEBITACCT like '%-54680' and transdate > '31/DEC/2014';--67--73 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54685','660090') where GLDEBITACCT like '%-54685' and transdate > '31/DEC/2014';--3--3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where GLDEBITACCT like '%-54705' and transdate > '31/DEC/2014';--21--28 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54725','665010') where GLDEBITACCT like '%-54725' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54740','660050') where GLDEBITACCT like '%-54740' and transdate > '31/DEC/2014';--6--14 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54750','600500') where GLDEBITACCT like '%-54750' and transdate > '31/DEC/2014';--7--8 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where GLDEBITACCT like '%-54760' and transdate > '31/DEC/2014';--24--30 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where GLDEBITACCT like '%-54780' and transdate > '31/DEC/2014';--69--75 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54790','669500') where GLDEBITACCT like '%-54790' and transdate > '31/DEC/2014';--1--3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where GLDEBITACCT like '%-54810' and transdate > '31/DEC/2014';--31--39 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where GLDEBITACCT like '%-54820' and transdate > '31/DEC/2014';--7--9 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'54850','630440') where GLDEBITACCT like '%-54850' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where GLDEBITACCT like '%-55110' and transdate > '31/DEC/2014';--20--25 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55210','621000') where GLDEBITACCT like '%-55210' and transdate > '31/DEC/2014';--6--9 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where GLDEBITACCT like '%-55320' and transdate > '31/DEC/2014';--36--42 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'55340','620120') where GLDEBITACCT like '%-55340' and transdate > '31/DEC/2014';--2--7 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56114','610114') where GLDEBITACCT like '%-56114' and transdate > '31/DEC/2014';--2--2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where GLDEBITACCT like '%-56131' and transdate > '31/DEC/2014';--21--27 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where GLDEBITACCT like '%-56134' and transdate > '31/DEC/2014';--15--24 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56151','520000') where GLDEBITACCT like '%-56151' and transdate > '31/DEC/2014';--62--75 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56152','610128') where GLDEBITACCT like '%-56152' and transdate > '31/DEC/2014';--7--8 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where GLDEBITACCT like '%-56170' and transdate > '31/DEC/2014';--3--4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56210','610220') where GLDEBITACCT like '%-56210' and transdate > '31/DEC/2014';--28--41 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56310','610310') where GLDEBITACCT like '%-56310' and transdate > '31/DEC/2014';--4--5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56410','630180') where GLDEBITACCT like '%-56410' and transdate > '31/DEC/2014';--21--26 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where GLDEBITACCT like '%-56510' and transdate > '31/DEC/2014';--24--33 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56520','510100') where GLDEBITACCT like '%-56520' and transdate > '31/DEC/2014';--92--107 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--12--17
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'56710','620120') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '8%');--10--10 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where GLDEBITACCT like '%-57110' and transdate > '31/DEC/2014';--31--41 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where GLDEBITACCT like '%-57120' and transdate > '31/DEC/2014';--63--78 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where GLDEBITACCT like '%-57210' and transdate > '31/DEC/2014';--41--47 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where GLDEBITACCT like '%-57310' and transdate > '31/DEC/2014';--9--9 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where GLDEBITACCT like '%-57320' and transdate > '31/DEC/2014';--1--1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57415','630220') where GLDEBITACCT like '%-57415' and transdate > '31/DEC/2014';--40--42 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57420','630200') where GLDEBITACCT like '%-57420' and transdate > '31/DEC/2014';--11--13 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where GLDEBITACCT like '%-57430' and transdate > '31/DEC/2014';--12--14 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57520','630200') where GLDEBITACCT like '%-57520' and transdate > '31/DEC/2014';--8--10 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where GLDEBITACCT like '%-57530' and transdate > '31/DEC/2014';--51--69 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where GLDEBITACCT like '%-57610' and transdate > '31/DEC/2014';--64--104 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where GLDEBITACCT like '%-57630' and transdate > '31/DEC/2014';--27--32 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57640','650040') where GLDEBITACCT like '%-57640' and transdate > '31/DEC/2014';--5--5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57660','650050') where GLDEBITACCT like '%-57660' and transdate > '31/DEC/2014';--3--4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where GLDEBITACCT like '%-57710' and transdate > '31/DEC/2014';--9--10 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'57720','630140') where GLDEBITACCT like '%-57720' and transdate > '31/DEC/2014';--6--7 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where GLDEBITACCT like '%-60100' and transdate > '31/DEC/2014';--5--7 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'80130','750010') where GLDEBITACCT like '%-80130' and transdate > '31/DEC/2014';--4--5 rows updated.
commit;
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'21261','146000') where WOGLACCOUNT like '%-21261' and transdate > '31/DEC/2014';--16--20 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'41200','500600') where WOGLACCOUNT like '%-41200' and transdate > '31/DEC/2014';--32--42 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'51390','600210') where WOGLACCOUNT like '%-51390' and transdate > '31/DEC/2014';--168--206 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'51510','600500') where WOGLACCOUNT like '%-51510' and transdate > '31/DEC/2014';--634--802 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'51520','600010') where WOGLACCOUNT like '%-51520' and transdate > '31/DEC/2014';--32--46 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'52100','620010') where WOGLACCOUNT like '%-52100' and transdate > '31/DEC/2014';--1568--2,028 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'53100','621000') where WOGLACCOUNT like '%-53100' and transdate > '31/DEC/2014';--1692--2,130 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'53120','621010') where WOGLACCOUNT like '%-53120' and transdate > '31/DEC/2014';--54--76 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'53170','621050') where WOGLACCOUNT like '%-53170' and transdate > '31/DEC/2014';--1958--2,844 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54100','669500') where WOGLACCOUNT like '%-54100' and transdate > '31/DEC/2014';--88--108 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54140','669700') where WOGLACCOUNT like '%-54140' and transdate > '31/DEC/2014';--50--62 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54150','660060') where WOGLACCOUNT like '%-54150' and transdate > '31/DEC/2014';--6--6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54210','530100') where WOGLACCOUNT like '%-54210' and transdate > '31/DEC/2014' AND WOGLACCOUNT NOT like '6%';--12--22 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54310','662020') where WOGLACCOUNT like '%-54310' and transdate > '31/DEC/2014';--18--18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54320','662010') where WOGLACCOUNT like '%-54320' and transdate > '31/DEC/2014';--202--244 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54330','669010') where WOGLACCOUNT like '%-54330' and transdate > '31/DEC/2014';--138--148 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54340','664040') where WOGLACCOUNT like '%-54340' and transdate > '31/DEC/2014';--90--90 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54360','662110') where WOGLACCOUNT like '%-54360' and transdate > '31/DEC/2014';--26--26 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54440','620030') where WOGLACCOUNT like '%-54440' and transdate > '31/DEC/2014';--1120--1,334 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54560','660210') where WOGLACCOUNT like '%-54560' and transdate > '31/DEC/2014';--60--60 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54570','664060') where WOGLACCOUNT like '%-54570' and transdate > '31/DEC/2014';--1954--2,340 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54580','630180') where WOGLACCOUNT like '%-54580' and transdate > '31/DEC/2014';--6--6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54610','660060') where WOGLACCOUNT like '%-54610' and transdate > '31/DEC/2014';--16--16 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54650','660030') where WOGLACCOUNT like '%-54650' and transdate > '31/DEC/2014';--182--260 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54660','660010') where WOGLACCOUNT like '%-54660' and transdate > '31/DEC/2014';--6--12 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54670','660025') where WOGLACCOUNT like '%-54670' and transdate > '31/DEC/2014';--6--12 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54680','660040') where WOGLACCOUNT like '%-54680' and transdate > '31/DEC/2014';--212--254 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54685','660090') where WOGLACCOUNT like '%-54685' and transdate > '31/DEC/2014';--6--6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54705','665010') where WOGLACCOUNT like '%-54705' and transdate > '31/DEC/2014';--108--134 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54740','660050') where WOGLACCOUNT like '%-54740' and transdate > '31/DEC/2014';--10--60 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54750','600500') where WOGLACCOUNT like '%-54750' and transdate > '31/DEC/2014';--4--48 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54760','660250') where WOGLACCOUNT like '%-54760' and transdate > '31/DEC/2014';--158--188 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54780','660510') where WOGLACCOUNT like '%-54780' and transdate > '31/DEC/2014';--436--538 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54790','669500') where WOGLACCOUNT like '%-54790' and transdate > '31/DEC/2014';--6--14 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54810','667030') where WOGLACCOUNT like '%-54810' and transdate > '31/DEC/2014';--170--210 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54820','667010') where WOGLACCOUNT like '%-54820' and transdate > '31/DEC/2014';--100--130 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54850','630440') where WOGLACCOUNT like '%-54850' and transdate > '31/DEC/2014';--6--6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'54990','620010') where WOGLACCOUNT like '%-54990' and transdate > '31/DEC/2014';--8--30 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'55110','669210') where WOGLACCOUNT like '%-55110' and transdate > '31/DEC/2014';--142--158 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'55210','621000') where WOGLACCOUNT like '%-55210' and transdate > '31/DEC/2014';--32--36 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'55320','660040') where WOGLACCOUNT like '%-55320' and transdate > '31/DEC/2014';--202--274 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'55340','620120') where WOGLACCOUNT like '%-55340' and transdate > '31/DEC/2014';--12--32 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56111','610108') where WOGLACCOUNT like '%-56111' and transdate > '31/DEC/2014';--4--10 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56112','610110') where WOGLACCOUNT like '%-56112' and transdate > '31/DEC/2014';--6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56113','610112') where WOGLACCOUNT like '%-56113' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56114','610114') where WOGLACCOUNT like '%-56114' and transdate > '31/DEC/2014';--20--20 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56131','610118') where WOGLACCOUNT like '%-56131' and transdate > '31/DEC/2014';--132--172 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56132','610120') where WOGLACCOUNT like '%-56132' and transdate > '31/DEC/2014';--124--148 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56133','610122') where WOGLACCOUNT like '%-56133' and transdate > '31/DEC/2014';--166--206 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56134','610124') where WOGLACCOUNT like '%-56134' and transdate > '31/DEC/2014';--106--136 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56140','610102') where WOGLACCOUNT like '%-56140' and transdate > '31/DEC/2014';--44--52 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56151','520000') where WOGLACCOUNT like '%-56151' and transdate > '31/DEC/2014';--372--450 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56152','610128') where WOGLACCOUNT like '%-56152' and transdate > '31/DEC/2014';--42--44 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56170','610130') where WOGLACCOUNT like '%-56170' and transdate > '31/DEC/2014';--232--268 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56210','610220') where WOGLACCOUNT like '%-56210' and transdate > '31/DEC/2014';--76--76 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56310','610310') where WOGLACCOUNT like '%-56310' and transdate > '31/DEC/2014';--24--26 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56510','610510') where WOGLACCOUNT like '%-56510' and transdate > '31/DEC/2014';--134--200 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56520','510100') where WOGLACCOUNT like '%-56520' and transdate > '31/DEC/2014';--592--662 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56710','620010') where WOGLACCOUNT like '%-56710' and transdate > '31/DEC/2014' AND (WOGLACCOUNT LIKE '6%' OR WOGLACCOUNT LIKE '7%');--330--392 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'56710','620120') where WOGLACCOUNT like '%-56710' and transdate > '31/DEC/2014' AND (WOGLACCOUNT LIKE '8%');--58--58 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57110','630010') where WOGLACCOUNT like '%-57110' and transdate > '31/DEC/2014';--128--178 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57120','630020') where WOGLACCOUNT like '%-57120' and transdate > '31/DEC/2014';--366--456 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57210','630100') where WOGLACCOUNT like '%-57210' and transdate > '31/DEC/2014';--316--348 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57310','630300') where WOGLACCOUNT like '%-57310' and transdate > '31/DEC/2014';--96--110 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57320','630310') where WOGLACCOUNT like '%-57320' and transdate > '31/DEC/2014';--160--178 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57410','630230') where WOGLACCOUNT like '%-57410' and transdate > '31/DEC/2014';--2 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57415','630220') where WOGLACCOUNT like '%-57415' and transdate > '31/DEC/2014';--196--248 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57420','630200') where WOGLACCOUNT like '%-57420' and transdate > '31/DEC/2014';--62--76 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57430','630210') where WOGLACCOUNT like '%-57430' and transdate > '31/DEC/2014';--154--164 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57520','630200') where WOGLACCOUNT like '%-57520' and transdate > '31/DEC/2014';--44--56 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57530','630200') where WOGLACCOUNT like '%-57530' and transdate > '31/DEC/2014';--306--414 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57610','650010') where WOGLACCOUNT like '%-57610' and transdate > '31/DEC/2014';--358--554 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57630','650030') where WOGLACCOUNT like '%-57630' and transdate > '31/DEC/2014';--120--144 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57640','650040') where WOGLACCOUNT like '%-57640' and transdate > '31/DEC/2014';--30--30 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57660','650050') where WOGLACCOUNT like '%-57660' and transdate > '31/DEC/2014';--22--28 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57710','630150') where WOGLACCOUNT like '%-57710' and transdate > '31/DEC/2014';--40--46 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'57720','630140') where WOGLACCOUNT like '%-57720' and transdate > '31/DEC/2014';--34--40 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'60100','669900') where WOGLACCOUNT like '%-60100' and transdate > '31/DEC/2014';--36--48 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'80130','750010') where WOGLACCOUNT like '%-80130' and transdate > '31/DEC/2014';--14--18 rows updated.
commit;
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'11460','124100') where GLDEBITACCT like '%-11460' and transdate > '31/DEC/2014';--4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where GLDEBITACCT like '%-11510' and transdate > '31/DEC/2014';--25866--27,330 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'11512','128040') where GLDEBITACCT like '%-11512' and transdate > '31/DEC/2014';--370--496 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'11910','145000') where GLDEBITACCT like '%-11910' and transdate > '31/DEC/2014';--2--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'21120','202000') where GLDEBITACCT like '%-21120' and transdate > '31/DEC/2014';--13064--16,184 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'21261','146000') where GLDEBITACCT like '%-21261' and transdate > '31/DEC/2014';--17320--21,508 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'21610','242800') where GLDEBITACCT like '%-21610' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'41200','500600') where GLDEBITACCT like '%-41200' and transdate > '31/DEC/2014';--20--24 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'41505','540100') where GLDEBITACCT like '%-41505' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51220','600140') where GLDEBITACCT like '%-51220' and transdate > '31/DEC/2014';--16--20 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51390','600210') where GLDEBITACCT like '%-51390' and transdate > '31/DEC/2014';--114--136 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where GLDEBITACCT like '%-51510' and transdate > '31/DEC/2014';--230--304 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51520','600010') where GLDEBITACCT like '%-51520' and transdate > '31/DEC/2014';--10--16 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51525','600210') where GLDEBITACCT like '%-51525' and transdate > '31/DEC/2014';--4--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where GLDEBITACCT like '%-52100' and transdate > '31/DEC/2014';--6528--8,508 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'52205','620090') where GLDEBITACCT like '%-52205' and transdate > '31/DEC/2014';--108--114 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where GLDEBITACCT like '%-53100' and transdate > '31/DEC/2014';--3870--5,032 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where GLDEBITACCT like '%-53120' and transdate > '31/DEC/2014';--1790--2,388 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'53160','621020') where GLDEBITACCT like '%-53160' and transdate > '31/DEC/2014';--20--30 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where GLDEBITACCT like '%-53170' and transdate > '31/DEC/2014';--932--1,326 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where GLDEBITACCT like '%-54100' and transdate > '31/DEC/2014';--36--40 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54130','669700') where GLDEBITACCT like '%-54130' and transdate > '31/DEC/2014';--10--14 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54135','669700') where GLDEBITACCT like '%-54135' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54140','669700') where GLDEBITACCT like '%-54140' and transdate > '31/DEC/2014';--18--22 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54150','660060') where GLDEBITACCT like '%-54150' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54210','530100') where GLDEBITACCT like '%-54210' and transdate > '31/DEC/2014' AND GLDEBITACCT NOT like '6%';--4--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54210','610020') where GLDEBITACCT like '%-54210' and transdate > '31/DEC/2014' AND GLDEBITACCT like '6%';--8--10 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54220','610410') where GLDEBITACCT like '%-54220' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54310','662020') where GLDEBITACCT like '%-54310' and transdate > '31/DEC/2014';--8--8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54320','662010') where GLDEBITACCT like '%-54320' and transdate > '31/DEC/2014';--120--156 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54330','669010') where GLDEBITACCT like '%-54330' and transdate > '31/DEC/2014';--46--52 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where GLDEBITACCT like '%-54340' and transdate > '31/DEC/2014';--34--34 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54360','662110') where GLDEBITACCT like '%-54360' and transdate > '31/DEC/2014';--4--4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where GLDEBITACCT like '%-54440' and transdate > '31/DEC/2014';--472--562 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where GLDEBITACCT like '%-54560' and transdate > '31/DEC/2014';--20--20 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where GLDEBITACCT like '%-54570' and transdate > '31/DEC/2014';--862--1,048 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where GLDEBITACCT like '%-54580' and transdate > '31/DEC/2014';--28--30 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where GLDEBITACCT like '%-54610' and transdate > '31/DEC/2014';--8--10 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where GLDEBITACCT like '%-54650' and transdate > '31/DEC/2014';--84--114 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54660','660010') where GLDEBITACCT like '%-54660' and transdate > '31/DEC/2014';--2--4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where GLDEBITACCT like '%-54670' and transdate > '31/DEC/2014';--2--4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54680','660040') where GLDEBITACCT like '%-54680' and transdate > '31/DEC/2014';--134--146 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54685','660090') where GLDEBITACCT like '%-54685' and transdate > '31/DEC/2014';--6--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where GLDEBITACCT like '%-54705' and transdate > '31/DEC/2014';--44--58 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54725','665010') where GLDEBITACCT like '%-54725' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54740','660050') where GLDEBITACCT like '%-54740' and transdate > '31/DEC/2014';--12--28 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54750','600500') where GLDEBITACCT like '%-54750' and transdate > '31/DEC/2014';--14--16 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where GLDEBITACCT like '%-54760' and transdate > '31/DEC/2014';--6--82 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where GLDEBITACCT like '%-54780' and transdate > '31/DEC/2014';--198--228 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54790','669500') where GLDEBITACCT like '%-54790' and transdate > '31/DEC/2014';--2--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where GLDEBITACCT like '%-54810' and transdate > '31/DEC/2014';--74--94 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where GLDEBITACCT like '%-54820' and transdate > '31/DEC/2014';--32--46 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54850','630440') where GLDEBITACCT like '%-54850' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'54990','620010') where GLDEBITACCT like '%-54990' and transdate > '31/DEC/2014';--8--18 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where GLDEBITACCT like '%-55110' and transdate > '31/DEC/2014';--50--60 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'55210','621000') where GLDEBITACCT like '%-55210' and transdate > '31/DEC/2014';--12--18 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where GLDEBITACCT like '%-55320' and transdate > '31/DEC/2014';--72--90 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'55340','620120') where GLDEBITACCT like '%-55340' and transdate > '31/DEC/2014';--4--14 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56111','610108') where GLDEBITACCT like '%-56111' and transdate > '31/DEC/2014';--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56112','610110') where GLDEBITACCT like '%-56112' and transdate > '31/DEC/2014';--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56113','610112') where GLDEBITACCT like '%-56113' and transdate > '31/DEC/2014';--2--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56114','610114') where GLDEBITACCT like '%-56114' and transdate > '31/DEC/2014';--4--4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where GLDEBITACCT like '%-56131' and transdate > '31/DEC/2014';--60--76 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56132','610120') where GLDEBITACCT like '%-56132' and transdate > '31/DEC/2014';--64--76 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56133','610122') where GLDEBITACCT like '%-56133' and transdate > '31/DEC/2014';--88--108 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where GLDEBITACCT like '%-56134' and transdate > '31/DEC/2014';--38--56 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56140','610102') where GLDEBITACCT like '%-56140' and transdate > '31/DEC/2014';--24--28 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56151','520000') where GLDEBITACCT like '%-56151' and transdate > '31/DEC/2014';--124--150 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56152','610128') where GLDEBITACCT like '%-56152' and transdate > '31/DEC/2014';--14--16 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where GLDEBITACCT like '%-56170' and transdate > '31/DEC/2014';--106--118 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56210','610220') where GLDEBITACCT like '%-56210' and transdate > '31/DEC/2014';--96--122 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56310','610310') where GLDEBITACCT like '%-56310' and transdate > '31/DEC/2014';--8--10 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56410','630180') where GLDEBITACCT like '%-56410' and transdate > '31/DEC/2014';--42--52 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where GLDEBITACCT like '%-56510' and transdate > '31/DEC/2014';--48--66 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56520','510100') where GLDEBITACCT like '%-56520' and transdate > '31/DEC/2014';--184--214 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--140--164 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620120') where GLDEBITACCT like '%-56710' and transdate > '31/DEC/2014' AND (GLDEBITACCT LIKE '8%');--20--20 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where GLDEBITACCT like '%-57110' and transdate > '31/DEC/2014';--62--82 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where GLDEBITACCT like '%-57120' and transdate > '31/DEC/2014';--126--156 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where GLDEBITACCT like '%-57210' and transdate > '31/DEC/2014';--124--136 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where GLDEBITACCT like '%-57310' and transdate > '31/DEC/2014';--42--44 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where GLDEBITACCT like '%-57320' and transdate > '31/DEC/2014';--72--78 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where GLDEBITACCT like '%-57410' and transdate > '31/DEC/2014';--2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57415','630220') where GLDEBITACCT like '%-57415' and transdate > '31/DEC/2014';--80--84 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57420','630200') where GLDEBITACCT like '%-57420' and transdate > '31/DEC/2014';--22--26 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where GLDEBITACCT like '%-57430' and transdate > '31/DEC/2014';--60--66 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57520','630200') where GLDEBITACCT like '%-57520' and transdate > '31/DEC/2014';--16--20 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where GLDEBITACCT like '%-57530' and transdate > '31/DEC/2014';--102--138 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where GLDEBITACCT like '%-57610' and transdate > '31/DEC/2014';--128--208 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where GLDEBITACCT like '%-57630' and transdate > '31/DEC/2014';--54--64 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57640','650040') where GLDEBITACCT like '%-57640' and transdate > '31/DEC/2014';--10--10 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57660','650050') where GLDEBITACCT like '%-57660' and transdate > '31/DEC/2014';--6--8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where GLDEBITACCT like '%-57710' and transdate > '31/DEC/2014';--18--20 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'57720','630140') where GLDEBITACCT like '%-57720' and transdate > '31/DEC/2014';--12--14 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where GLDEBITACCT like '%-60100' and transdate > '31/DEC/2014';--12--16 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'80130','750010') where GLDEBITACCT like '%-80130' and transdate > '31/DEC/2014';--8--10 rows updated.
commit;
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where GLCREDITACCT like '%-11510' and transdate > '31/DEC/2014';--3362--4,386 rows updated.
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'11512','128040') where GLCREDITACCT like '%-11512' and transdate > '31/DEC/2014';--312--432 rows updated.
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where GLCREDITACCT like '%-21120' and transdate > '31/DEC/2014';--36858--46,420 rows updated.
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'52200','620015') where GLCREDITACCT like '%-52200' and transdate > '31/DEC/2014';--22764--23,600 rows updated.
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'52205','620090') where GLCREDITACCT like '%-52205' and transdate > '31/DEC/2014';--1348--1,588 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'11455','128025') where SUNGLACCOUNT = '11455';--4--4 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'11460','124100') where SUNGLACCOUNT = '11460';--5--7 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'11510','128010') where SUNGLACCOUNT = '11510';--29257--30,501 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'11512','128040') where SUNGLACCOUNT = '11512';--1635--1,758 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'11910','145000') where SUNGLACCOUNT = '11910';--70--72 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'15590','192900') where SUNGLACCOUNT = '15590';--250--250 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'21120','202000') where SUNGLACCOUNT = '21120';--115199--121,540 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'21152','202150') where SUNGLACCOUNT = '21152';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'21261','146000') where SUNGLACCOUNT = '21261';--39232--41,326 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'21610','242800') where SUNGLACCOUNT = '21610';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'41100','400100') where SUNGLACCOUNT = '41100';--5--5 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'41200','500600') where SUNGLACCOUNT = '41200';--60--62 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'41505','540100') where SUNGLACCOUNT = '41505';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51110','600010') where SUNGLACCOUNT = '51110';--7--7 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51220','600140') where SUNGLACCOUNT = '51220';--32--34 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51250','600110') where SUNGLACCOUNT = '51250';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51260','600120') where SUNGLACCOUNT = '51260';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51350','600050') where SUNGLACCOUNT = '51350';--45--45 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51390','600210') where SUNGLACCOUNT = '51390';--108--119 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51510','600500') where SUNGLACCOUNT = '51510';--521--558 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51520','600010') where SUNGLACCOUNT = '51520';--49--52 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'51525','600210') where SUNGLACCOUNT = '51525';--23--24 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'52100','620010') where SUNGLACCOUNT = '52100';--15706--16,696 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'52200','620015') where SUNGLACCOUNT = '52200';--13464--13,882 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'52205','620090') where SUNGLACCOUNT = '52205';--1421--1,544 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'53100','621000') where SUNGLACCOUNT = '53100';--10356--10,937 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'53120','621010') where SUNGLACCOUNT = '53120';--4175--4,474 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'53160','621020') where SUNGLACCOUNT = '53160';--55--60 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'53170','621050') where SUNGLACCOUNT = '53170';--2609--2,806 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54100','669500') where SUNGLACCOUNT = '54100';--60--62 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54130','669700') where SUNGLACCOUNT = '54130';--26--28 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54135','669700') where SUNGLACCOUNT = '54135';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54140','669700') where SUNGLACCOUNT = '54140';--38--40 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54150','660060') where SUNGLACCOUNT = '54150';--17--17 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54210','530100') where SUNGLACCOUNT = '54210' AND SUNCOSTCENTRE NOT like '6%';--14--15 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54210','610020') where SUNGLACCOUNT = '54210' AND SUNCOSTCENTRE like '6%';--17--18 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54220','610410') where SUNGLACCOUNT = '54220';--4--4 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54310','662020') where SUNGLACCOUNT = '54310';--29--29 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54320','662010') where SUNGLACCOUNT = '54320';--281--299 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54330','669010') where SUNGLACCOUNT = '54330';--78--81 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54340','664040') where SUNGLACCOUNT = '54340';--240--240 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54360','662110') where SUNGLACCOUNT = '54360';--16--16 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54440','620030') where SUNGLACCOUNT = '54440';--1152--1,197 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54450','620010') where SUNGLACCOUNT = '54450';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54560','660210') where SUNGLACCOUNT = '54560';--106--106 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54570','664060') where SUNGLACCOUNT = '54570';--2007--2,100 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54580','630180') where SUNGLACCOUNT = '54580';--66--67 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54610','660060') where SUNGLACCOUNT = '54610';--13--14 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54650','660030') where SUNGLACCOUNT = '54650';--251--266 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54660','660010') where SUNGLACCOUNT = '54660';--6--7 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54670','660025') where SUNGLACCOUNT = '54670';--10--11 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54680','660040') where SUNGLACCOUNT = '54680';--273--279 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54685','660090') where SUNGLACCOUNT = '54685';--14--14 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54705','665010') where SUNGLACCOUNT = '54705';--189--196 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54720','669050') where SUNGLACCOUNT = '54720';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54725','665010') where SUNGLACCOUNT = '54725';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54740','660050') where SUNGLACCOUNT = '54740';--144--152 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54750','600500') where SUNGLACCOUNT = '54750';--28--29 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54751','600500') where SUNGLACCOUNT = '54751';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54760','660250') where SUNGLACCOUNT = '54760';--139--139 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54780','660510') where SUNGLACCOUNT = '54780';--462--477 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54790','669500') where SUNGLACCOUNT = '54790';--3--5 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54810','667030') where SUNGLACCOUNT = '54810';--232--242 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54820','667010') where SUNGLACCOUNT = '54820';--186--193 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54850','630440') where SUNGLACCOUNT = '54850';--5--5 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'54990','620010') where SUNGLACCOUNT = '54990';--6--11 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'55110','669210') where SUNGLACCOUNT = '55110';--136--141 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'55210','621000') where SUNGLACCOUNT = '55210';--41--44 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'55310','660080') where SUNGLACCOUNT = '55310';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'55320','660040') where SUNGLACCOUNT = '55320';--121--130 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'55340','620120') where SUNGLACCOUNT = '55340';--2--7 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56111','610108') where SUNGLACCOUNT = '56111';--16--17 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56112','610110') where SUNGLACCOUNT = '56112';--4--5 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56113','610112') where SUNGLACCOUNT = '56113';--13--13 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56114','610114') where SUNGLACCOUNT = '56114';--20--20 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56120','610116') where SUNGLACCOUNT = '56120';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56131','610118') where SUNGLACCOUNT = '56131';--127--135 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56132','610120') where SUNGLACCOUNT = '56132';--96--102 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56133','610122') where SUNGLACCOUNT = '56133';--157--167 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56134','610124') where SUNGLACCOUNT = '56134';--43--52 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56140','610102') where SUNGLACCOUNT = '56140';--35--37 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56151','520000') where SUNGLACCOUNT = '56151';--246--259 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56152','610128') where SUNGLACCOUNT = '56152';--71--72 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56160','610180') where SUNGLACCOUNT = '56160';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56170','610130') where SUNGLACCOUNT = '56170';--213--219 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56210','610220') where SUNGLACCOUNT = '56210';--136--149 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56310','610310') where SUNGLACCOUNT = '56310';--16--17 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56410','630180') where SUNGLACCOUNT = '56410';--89--94 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56510','610510') where SUNGLACCOUNT = '56510';--121--130 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56520','510100') where SUNGLACCOUNT = '56520';--147--162 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56710','620010') where SUNGLACCOUNT = '56710' AND (SUNCOSTCENTRE LIKE '6%' OR SUNCOSTCENTRE LIKE '7%');--525--537 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'56710','620120') where SUNGLACCOUNT = '56710' AND (SUNCOSTCENTRE LIKE '8%');--64--64 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57110','630010') where SUNGLACCOUNT = '57110';--171--181 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57120','630020') where SUNGLACCOUNT = '57120';--280--295 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57130','630040') where SUNGLACCOUNT = '57130';--5--5 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57140','630020') where SUNGLACCOUNT = '57140';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57210','630100') where SUNGLACCOUNT = '57210';--222--228 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57310','630300') where SUNGLACCOUNT = '57310';--43--44 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57320','630310') where SUNGLACCOUNT = '57320';--115--118 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57410','630230') where SUNGLACCOUNT = '57410';--18--19 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57415','630220') where SUNGLACCOUNT = '57415';--110--112 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57420','630200') where SUNGLACCOUNT = '57420';--86--88 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57430','630210') where SUNGLACCOUNT = '57430';--80--83 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57520','630200') where SUNGLACCOUNT = '57520';--37--39 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57530','630200') where SUNGLACCOUNT = '57530';--252--270 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57610','650010') where SUNGLACCOUNT = '57610';--362--402 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57620','650020') where SUNGLACCOUNT = '57620';--2--2 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57630','650030') where SUNGLACCOUNT = '57630';--122--127 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57640','650040') where SUNGLACCOUNT = '57640';--30--30 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57660','650050') where SUNGLACCOUNT = '57660';--21--22 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57710','630150') where SUNGLACCOUNT = '57710';--47--48 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57720','630140') where SUNGLACCOUNT = '57720';--14--15 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'57810','630110') where SUNGLACCOUNT = '57810';--1--1 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'60100','669900') where SUNGLACCOUNT = '60100';--13--15 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'80120','750041') where SUNGLACCOUNT = '80120';--3--3 rows updated.
update SUNGLTXN_IFACE set SUNGLACCOUNT = replace(SUNGLACCOUNT,'80130','750010') where SUNGLACCOUNT = '80130';--12--13 rows updated.
commit;
update INVOICE set TAX1GL = replace(TAX1GL,'21261','146000') where historyflag=0 AND TAX1GL like '%-21261'; -- 15 --7 rows--7 rows updated.
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-53120'; -- 203 --130--130
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'53160','621020') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-53160'; -- 1 --0-------
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-53170'; -- 5 --3--3 rows
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'54320','662010') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-54320'; -- 9 rows-------
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'56210','610220') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-56210'; --1 row--1 rows
update INVOICECOST set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLCREDITACCT like '%-21120'; -- 222 --135 --135
update POCOST set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-11510'; -- 501 --579 rows--579 rows updated.
update POCOST set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-21120'; --8560 --8748 rows updated--8,748 rows
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'11460','124100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-11460'; --1 row--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-11510'; -- 1471 --1456 --1,456 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'21261','146000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-21261'; --2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'41200','500600') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-41200'; --3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51110','600010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51110'; --1 --0 rows-------
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51390','600210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51390';--15--14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51510';--46 --45--45 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51520','600010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51520';--3 --4--4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-52100';--1717 --1742--1,742 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53100';--2573 --2744--2,744 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53120';--6 --3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53170';--325 --389--389 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54100';--9 --8--8 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54140','669700') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54140';--2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54150','660060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54150';--1 --2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54210','530100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54210' AND GLDEBITACCT NOT like '6%';--5--5 rows
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54310','662020') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54310';--2 --3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54320','662010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54320';--14 --13--13 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54330','669010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54330';--5--5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54340';--23 --28--28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54360','662110') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54360';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54440';--199 --193--193 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54560';--1 --3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54570';--116 --132--132 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54580';--1 --2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54610';--3 --1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54650';--28 --26--26 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54660','660010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54660';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54670';--2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54680','660040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54680';--25--25 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54685','660090') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54685';--3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54705';--33--33 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54740','660050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54740';--15 --19--19 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54750','600500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54750';--2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54760';--30--30 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54780';--55 --52--52 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54790','669500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54790';--1 --2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54810';--23 --26--26 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54820';--18 --20--20 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54850','630440') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54850';--2--2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'54990','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54990';--4--4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55110';--38 --42--42 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'55210','621000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55210';--5 --11--11 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55320';--23 --28--28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'55340','620120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55340';--10 --13--13 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56111','610108') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56111';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56112','610110') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56112';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56114','610114') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56114';--1-------
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56131';--11 --12--12 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56132','610120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56132';--16 --18--18 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56133','610122') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56133';--32 --35--35 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56134';--9 --7--7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56140','610102') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56140';--7--7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56151','520000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56151';--2 --3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56152','610128') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56152';--5--5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56170';--44 --43--43 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56310','610310') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56310';--3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56510';--30 --34--34 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56520','510100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56520';--5--5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--104 --109--109 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'56710','620120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '8%');--14--14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57110';--23 --25--25 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57120';--25 --23--23 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57130','630040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57130';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57210';--44 --34--34 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57310';--30 --28--28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57320';--27 --28--28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57410';--1 --7--7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57415','630220') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57415';--12 --15--15 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57420','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57420';--15 --16--16 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57430';--23 --24--24 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57520','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57520';--3--3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57530';--7 --6--6 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57610';--75 --80--80 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57630';--26 --28--28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57640','650040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57640';--7 --6--6 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57660','650050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57660';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57710';--31--31 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'57720','630140') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57720';--5 --6--6 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-60100';--7 --5--5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'80120','750041') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-80120';--1--1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'80130','750010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-80130';--1--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'11460','124100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-11460'; --1 --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-11510';--1471 --1456--1,456 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'21261','146000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-21261';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'41200','500600') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-41200'; --3--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'51390','600210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51390'; --14--14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51510'; --45--45 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'51520','600010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51520'; --4--4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-52100'; --1742--1,742 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53100'; --2744--2,744 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'53120','621010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53120'; --3--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-53170'; --389--389 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54100','669500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54100'; --8--8 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54140','669700') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54140'; --2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54150','660060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54150'; --2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54210','530100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54210' AND GLDEBITACCT NOT like '6%'; --5--5 rows
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54310','662020') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54310';--3--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54320','662010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54320';--13--13 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54330','669010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54330';--5--5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54340';--28--28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54360','662110') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54360';--1--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54440';--193--193 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54560','660210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54560';--3--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54570';--132--132 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54580','630180') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54580';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54610','660060') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54610';--1--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54650';--26--26 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54660','660010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54660';--1--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54670';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54680','660040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54680';--25--25 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54685','660090') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54685';--3--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54705','665010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54705';--33--33 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54740','660050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54740';--19--19 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54750','600500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54750';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54760','660250') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54760';--30--30 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54780','660510') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54780';--52--52 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54790','669500') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54790';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54810';--26--26 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54820';--20--20 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54850','630440') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54850';--2--2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'54990','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-54990';--4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55110';--42 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'55210','621000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55210';--11 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55320';--28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'55340','620120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-55340';--13 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56111','610108') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56111';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56112','610110') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56112';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56131';--12 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56132','610120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56132';--18 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56133','610122') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56133';--35 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56134','610124') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56134';--7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56140','610102') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56140';--7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56151','520000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56151';--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56152','610128') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56152';--5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56170';--43 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56310','610310') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56310';--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56510';--34 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56520','510100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56520';--5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--109 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620120') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '8%');--14 rows
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57110';--25 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57120';--23 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57130','630040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57130';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57210';--34 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57310';--28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57320';--28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57410';--7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57415','630220') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57415';--15 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57420','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57420';--16 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57430';--24 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57520','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57520';--3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57530';--6 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57610';--80 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57630';--28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57640','650040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57640';--6 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57660','650050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57660';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57710';--31 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'57720','630140') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-57720';--6 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'60100','669900') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-60100';--7 (finish the rest)--5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'80120','750041') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-80120';--1--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'80130','750010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-80130';--1--1 rows updated.
update POLINE set GLCREDITACCT = replace(GLCREDITACCT,'11510','128010') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-11510'; --501--579 rows updated.
update POLINE set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-21120';-- 8559--8,748 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-11510';--309--351 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-51510';--4--3 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-52100';--444--493 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-53100';--255--259 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-53170';--160--180 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54340';--4--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54440';--39--49 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54570';--52--43 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54650';--2--2 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54670';--1--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54810';--7--6 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-54820';--3--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-55110';--4--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-55320';--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'56120','610116') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-56120';--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-56131';--1--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-56170';--4--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-56510';--1--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--90--89 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57110';--31--31 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57120';--3--3 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57140','630020') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57140';--1--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57310';--3--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57320';--8--7 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57410';--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57430';--1-------
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57530';--5--5 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57610';--24--24 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57630';--4--4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-57710';--13--13 rows updated.
update PRCOST set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLCREDITACCT like '%-21120';--1355--1,461 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-11510';--309--351 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'51510','600500') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-51510';--3 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-52100';--493 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-53100';--259 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-53170';--180 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54340';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54440';--49 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54570';--43 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54650','660030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54650';--2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54670','660025') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54670';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54810';--6 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-54820';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-55110';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'55320','660040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-55320';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'56120','610116') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-56120';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'56131','610118') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-56131';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-56170';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'56510','610510') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-56510';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--89 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57110','630010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57110';--31 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57120','630020') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57120';--3 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57140','630020') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57140';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57310','630300') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57310';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57320';--7 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57410','630230') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57410';--1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57530','630200') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57530';--5 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57610','650010') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57610';--24 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57630','650030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57630';--4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'57710','630150') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-57710';--13 (finish this)--13 rows updated.
update PRLINE set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLCREDITACCT like '%-21120';--1355--1,461 rows updated.
update QUOTATIONLINE set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLCREDITACCT like '%-21120';--853--886 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'11510','128010') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-11510';--373--406 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'52100','620010') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-52100';--303--320 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'53100','621000') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-53100';--205--209 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'53170','621050') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-53170';--124--118 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54340','664040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54340';--2--2 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54440','620030') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54440';--48--58 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54450','620010') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54450';--5--5 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54570','664060') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54570';--17--17 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54810','667030') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54810';--15--15 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'54820','667010') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-54820';--14--14 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'55110','669210') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-55110';--4--4 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'55310','660080') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-55310';--1--1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'56170','610130') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-56170';--1--1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'56710','620010') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-56710' AND (GLDEBITACCT LIKE '6%' OR GLDEBITACCT LIKE '7%');--13--13 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'57210','630100') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-57210';--7--7 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'57320','630310') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-57320';--8--7 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'57415','630220') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-57415';--1--1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'57430','630210') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-57430';--1-------
update RFQVENDOR set GLCREDITACCT = replace(GLCREDITACCT,'21120','202000') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLCREDITACCT like '%-21120';--296--314 rows updated.
commit;
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'11460','124100') where historyflag=0 and GLACCOUNT like '%-11460';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'11512','128040') where historyflag=0 and GLACCOUNT like '%-11512';--1--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'15590','192900') where historyflag=0 and GLACCOUNT like '%-15590';--1--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'21120','202000') where historyflag=0 and GLACCOUNT like '%-21120';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'21261','146000') where historyflag=0 and GLACCOUNT like '%-21261';--2--2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'41200','500600') where historyflag=0 and GLACCOUNT like '%-41200';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'51110','600010') where historyflag=0 and GLACCOUNT like '%-51110';--1-------
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'51390','600210') where historyflag=0 and GLACCOUNT like '%-51390';--22--26 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'51510','600500') where historyflag=0 and GLACCOUNT like '%-51510';--80--79 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'51520','600010') where historyflag=0 and GLACCOUNT like '%-51520';--7--8 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'52100','620010') where historyflag=0 and GLACCOUNT like '%-52100';--457--473 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'53100','621000') where historyflag=0 and GLACCOUNT like '%-53100';--628--617 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'53120','621010') where historyflag=0 and GLACCOUNT like '%-53120';--9--5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'53170','621050') where historyflag=0 and GLACCOUNT like '%-53170';--228--254 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54100','669500') where historyflag=0 and GLACCOUNT like '%-54100';--16--16 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54140','669700') where historyflag=0 and GLACCOUNT like '%-54140';--4--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54150','660060') where historyflag=0 and GLACCOUNT like '%-54150';--8--9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54210','530100') where historyflag=0 and GLACCOUNT like '%-54210' AND GLACCOUNT NOT like '6%';--4--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54310','662020') where historyflag=0 and GLACCOUNT like '%-54310';--2--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54320','662010') where historyflag=0 and GLACCOUNT like '%-54320';--14--13 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54330','669010') where historyflag=0 and GLACCOUNT like '%-54330';--19--16 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54340','664040') where historyflag=0 and GLACCOUNT like '%-54340';--65--71 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54360','662110') where historyflag=0 and GLACCOUNT like '%-54360';--10--10 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54440','620030') where historyflag=0 and GLACCOUNT like '%-54440';--275--290 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54450','620010') where historyflag=0 and GLACCOUNT like '%-54450';--1--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54560','660210') where historyflag=0 and GLACCOUNT like '%-54560';--7--9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54570','664060') where historyflag=0 and GLACCOUNT like '%-54570';--137--138 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54580','630180') where historyflag=0 and GLACCOUNT like '%-54580';--2--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54610','660060') where historyflag=0 and GLACCOUNT like '%-54610';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54650','660030') where historyflag=0 and GLACCOUNT like '%-54650';--37--38 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54660','660010') where historyflag=0 and GLACCOUNT like '%-54660';--7--7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54670','660025') where historyflag=0 and GLACCOUNT like '%-54670';--4--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54680','660040') where historyflag=0 and GLACCOUNT like '%-54680';--50--48 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54685','660090') where historyflag=0 and GLACCOUNT like '%-54685';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54705','665010') where historyflag=0 and GLACCOUNT like '%-54705';--149--152 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54740','660050') where historyflag=0 and GLACCOUNT like '%-54740';--86--93 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54750','600500') where historyflag=0 and GLACCOUNT like '%-54750';--3--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54760','660250') where historyflag=0 and GLACCOUNT like '%-54760';--31--32 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54780','660510') where historyflag=0 and GLACCOUNT like '%-54780';--73--77 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54790','669500') where historyflag=0 and GLACCOUNT like '%-54790';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54810','667030') where historyflag=0 and GLACCOUNT like '%-54810';--54--54 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54820','667010') where historyflag=0 and GLACCOUNT like '%-54820';--50--53 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54850','630440') where historyflag=0 and GLACCOUNT like '%-54850';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'54990','620010') where historyflag=0 and GLACCOUNT like '%-54990';--4--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'55110','669210') where historyflag=0 and GLACCOUNT like '%-55110';--75--78 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'55210','621000') where historyflag=0 and GLACCOUNT like '%-55210';--16--17 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'55320','660040') where historyflag=0 and GLACCOUNT like '%-55320';--49--51 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'55340','620120') where historyflag=0 and GLACCOUNT like '%-55340';--22--26 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56111','610108') where historyflag=0 and GLACCOUNT like '%-56111';--11--9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56112','610110') where historyflag=0 and GLACCOUNT like '%-56112';--2--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56113','610112') where historyflag=0 and GLACCOUNT like '%-56113';--5--4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56114','610114') where historyflag=0 and GLACCOUNT like '%-56114';--16--16 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56120','610116') where historyflag=0 and GLACCOUNT like '%-56120';--1--2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56131','610118') where historyflag=0 and GLACCOUNT like '%-56131';--19--20 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56132','610120') where historyflag=0 and GLACCOUNT like '%-56132';--37--39 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56133','610122') where historyflag=0 and GLACCOUNT like '%-56133';--63--66 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56134','610124') where historyflag=0 and GLACCOUNT like '%-56134';--6--6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56140','610102') where historyflag=0 and GLACCOUNT like '%-56140';--18--17 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56151','520000') where historyflag=0 and GLACCOUNT like '%-56151';--4--6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56152','610128') where historyflag=0 and GLACCOUNT like '%-56152';--5--5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56170','610130') where historyflag=0 and GLACCOUNT like '%-56170';--59--58 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56210','610220') where historyflag=0 and GLACCOUNT like '%-56210';--2--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56310','610310') where historyflag=0 and GLACCOUNT like '%-56310';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56510','610510') where historyflag=0 and GLACCOUNT like '%-56510';--83--92 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56520','510100') where historyflag=0 and GLACCOUNT like '%-56520';--7--7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56710','620010') where historyflag=0 and GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '6%' OR GLACCOUNT LIKE '7%');--654--678 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'56710','620120') where historyflag=0 and GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '8%');--25--25 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57110','630010') where historyflag=0 and GLACCOUNT like '%-57110';--37--39 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57120','630020') where historyflag=0 and GLACCOUNT like '%-57120';--50--51 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57130','630040') where historyflag=0 and GLACCOUNT like '%-57130';--2--2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57210','630100') where historyflag=0 and GLACCOUNT like '%-57210';--37--36 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57310','630300') where historyflag=0 and GLACCOUNT like '%-57310';--20--18 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57320','630310') where historyflag=0 and GLACCOUNT like '%-57320';--37--36 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57410','630230') where historyflag=0 and GLACCOUNT like '%-57410';--3--5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57415','630220') where historyflag=0 and GLACCOUNT like '%-57415';--16--19 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57420','630200') where historyflag=0 and GLACCOUNT like '%-57420';--53--55 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57430','630210') where historyflag=0 and GLACCOUNT like '%-57430';--31--34 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57520','630200') where historyflag=0 and GLACCOUNT like '%-57520';--3--3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57530','630200') where historyflag=0 and GLACCOUNT like '%-57530';--8--7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57610','650010') where historyflag=0 and GLACCOUNT like '%-57610';--142--149 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57630','650030') where historyflag=0 and GLACCOUNT like '%-57630';--104--109 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57640','650040') where historyflag=0 and GLACCOUNT like '%-57640';--31--31 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57660','650050') where historyflag=0 and GLACCOUNT like '%-57660';--2--2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57710','630150') where historyflag=0 and GLACCOUNT like '%-57710';--7--7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'57720','630140') where historyflag=0 and GLACCOUNT like '%-57720';--5--5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'60100','669900') where historyflag=0 and GLACCOUNT like '%-60100';--28--26 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'80120','750041') where historyflag=0 and GLACCOUNT like '%-80120';--1--1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'80130','750010') where historyflag=0 and GLACCOUNT like '%-80130';--9--10 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'11460','124100') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-11460';--24--24 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'11512','128040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-11512';--1--1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'15590','192900') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-15590';--5--5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'21120','202000') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-21120';--4--3 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'21261','146000') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-21261';--11--11 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'41200','500600') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-41200';--17--17 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51110','600010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51110';--13--4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51130','600190') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51130';--1-------
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51390','600210') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51390';--149--174 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51510','600500') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51510';--822--845 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51520','600010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51520';--49--55 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'52100','620010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-52100';--2298--2,372 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'53100','621000') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-53100';--4214--4,191 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'53120','621010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-53120';--49--31 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'53170','621050') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-53170';--1196--1,292 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54100','669500') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54100';--104--106 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54140','669700') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54140';--24--24 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54150','660060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54150';--55--59 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54210','530100') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54210' AND GLACCOUNT NOT like '6%';--24--25 rows 
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54310','662020') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54310';--13--18 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54320','662010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54320';--82--83 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54330','669010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54330';--133--112 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54340','664040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54340';--446--475 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54360','662110') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54360';--69--69 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54440','620030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54440';--1401--1,454 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54450','620010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54450';--3--3 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54560','660210') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54560';--49--57 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54570','664060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54570';--799--800 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54580','630180') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54580';--13--18 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54610','660060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54610';--21--21 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54650','660030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54650';--242--254 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54660','660010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54660';--45--46 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54670','660025') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54670';--23--25 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54680','660040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54680';--320--312 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54685','660090') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54685';--15--15 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54705','665010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54705';--1089--1,104 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54740','660050') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54740';--644--670 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54750','600500') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54750';--20--22 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54760','660250') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54760';--215--207 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54780','660510') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54780';--442--466 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54790','669500') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54790';--19--20 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54810','667030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54810';--295--303 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54820','667010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54820';--273--288 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54850','630440') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54850';--19--19 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'54990','620010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-54990';--16--17 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'55110','669210') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-55110';--500--516 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'55210','621000') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-55210';--91--112 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'55320','660040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-55320';--332--335 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'55330','620100') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-55330';--2--2 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'55340','620120') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-55340';--72--97 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56111','610108') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56111';--57--46 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56112','610110') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56112';--9--5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56113','610112') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56113';--24--18 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56114','610114') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56114';--119--119 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56120','610116') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56120';--6--7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56131','610118') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56131';--96--101 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56132','610120') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56132';--194--205 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56133','610122') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56133';--312--325 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56134','610124') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56134';--35--36 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56140','610102') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56140';--104--99 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56151','520000') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56151';--23--35 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56152','610128') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56152';--26--26 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56170','610130') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56170';--285--284 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56210','610220') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56210';--8--1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56310','610310') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56310';--18--18 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56510','610510') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56510';--613--665 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56520','510100') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56520';--46--46 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56710','620010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '6%' OR GLACCOUNT LIKE '7%');--1831--2,299 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'56710','620120') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-56710' AND (GLACCOUNT LIKE '8%');--126--126 rows
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57110','630010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57110';--280--293 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57120','630020') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57120';--352--363 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57130','630040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57130';--13--13 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57210','630100') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57210';--247--221 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57310','630300') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57310';--79--68 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57320','630310') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57320';--180--177 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57410','630230') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57410';--21--32 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57415','630220') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57415';--103--117 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57420','630200') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57420';--381--379 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57430','630210') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57430';--198--199 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57520','630200') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57520';--18--18 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57530','630200') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57530';--50--44 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57610','650010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57610';--973--1,017 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57630','650030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57630';--759--787 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57640','650040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57640';--217--217 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57660','650050') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57660';--13--13 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57710','630150') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57710';--44--44 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'57720','630140') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-57720';--32--36 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'60100','669900') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-60100';--110--92 rows updated
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'80120','750041') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-80120';--5--5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'80130','750010') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-80130';--72--78 rows updated.
commit;
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-51325';--2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--2 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51325';--84 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';--1 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'51325','600050') where WOGLACCOUNT like '%-51325' and transdate > '31/DEC/2014';--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';--2 rows updated.
update LABTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where GLDEBITACCT like '%-C88020501' and transdate > '31/DEC/2014'; --51 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where GLDEBITACCT like '%-C6MF14005' and transdate > '31/DEC/2014'; --9 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where GLDEBITACCT like '%-C6MF14041' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14043','161030') where GLDEBITACCT like '%-C6MF14043' and transdate > '31/DEC/2014'; --4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where GLDEBITACCT like '%-C6MF14060' and transdate > '31/DEC/2014'; --4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where GLDEBITACCT like '%-C6MF14064' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14065','161030') where GLDEBITACCT like '%-C6MF14065' and transdate > '31/DEC/2014'; --3 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where GLDEBITACCT like '%-C86900303' and transdate > '31/DEC/2014'; --11 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where GLDEBITACCT like '%-C86900401' and transdate > '31/DEC/2014'; --4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where GLDEBITACCT like '%-C86900402' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where GLDEBITACCT like '%-C86900403' and transdate > '31/DEC/2014'; --31 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where GLDEBITACCT like '%-C86900408' and transdate > '31/DEC/2014'; --23 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where GLDEBITACCT like '%-C86900409' and transdate > '31/DEC/2014'; --5 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where GLDEBITACCT like '%-C87520304' and transdate > '31/DEC/2014'; --2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where GLDEBITACCT like '%-C87520402' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530504','161040') where GLDEBITACCT like '%-C87530504' and transdate > '31/DEC/2014'; --2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where GLDEBITACCT like '%-C87560404' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where GLDEBITACCT like '%-C88020302' and transdate > '31/DEC/2014'; --2 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where GLDEBITACCT like '%-C88020402' and transdate > '31/DEC/2014'; --4 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where GLDEBITACCT like '%-C86900502' and transdate > '31/DEC/2014'; --17 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where GLDEBITACCT like '%-C88020501' and transdate > '31/DEC/2014'; --1 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where GLDEBITACCT like '%-C88020509' and transdate > '31/DEC/2014'; --7 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where GLDEBITACCT like '%-C6MF14005' and transdate > '31/DEC/2014'; --9 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where GLDEBITACCT like '%-C6MF14041' and transdate > '31/DEC/2014'; --2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14043','161030') where GLDEBITACCT like '%-C6MF14043' and transdate > '31/DEC/2014'; --4 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where GLDEBITACCT like '%-C6MF14060' and transdate > '31/DEC/2014'; --4 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where GLDEBITACCT like '%-C6MF14064' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14065','161030') where GLDEBITACCT like '%-C6MF14065' and transdate > '31/DEC/2014'; --3 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14071','161030') where GLDEBITACCT like '%-C6MF14071' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where GLDEBITACCT like '%-C86900303' and transdate > '31/DEC/2014'; --11 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where GLDEBITACCT like '%-C86900401' and transdate > '31/DEC/2014'; --4 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where GLDEBITACCT like '%-C86900402' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where GLDEBITACCT like '%-C86900403' and transdate > '31/DEC/2014'; --31 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where GLDEBITACCT like '%-C86900408' and transdate > '31/DEC/2014'; --23 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where GLDEBITACCT like '%-C86900409' and transdate > '31/DEC/2014'; --5 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87515402','161040') where GLDEBITACCT like '%-C87515402' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87515403','161040') where GLDEBITACCT like '%-C87515403' and transdate > '31/DEC/2014'; --3 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where GLDEBITACCT like '%-C87520304' and transdate > '31/DEC/2014'; --2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where GLDEBITACCT like '%-C87520402' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530504','161040') where GLDEBITACCT like '%-C87530504' and transdate > '31/DEC/2014'; --4 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where GLDEBITACCT like '%-C87560404' and transdate > '31/DEC/2014'; --1 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where GLDEBITACCT like '%-C88020302' and transdate > '31/DEC/2014'; --2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where GLDEBITACCT like '%-C88020402' and transdate > '31/DEC/2014'; --36 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where GLDEBITACCT like '%-C86900502' and transdate > '31/DEC/2014'; --17 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510504','161040') where GLDEBITACCT like '%-C87510504' and transdate > '31/DEC/2014'; --2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where GLDEBITACCT like '%-C88020501' and transdate > '31/DEC/2014'; --6 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where GLDEBITACCT like '%-C88020509' and transdate > '31/DEC/2014'; --22 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C23010401','151300') where GLDEBITACCT like '%-C23010401' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14000','161030') where GLDEBITACCT like '%-C6MF14000' and transdate > '31/DEC/2014'; --2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14001','161030') where GLDEBITACCT like '%-C6MF14001' and transdate > '31/DEC/2014'; --20
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where GLDEBITACCT like '%-C6MF14002' and transdate > '31/DEC/2014'; --62 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where GLDEBITACCT like '%-C6MF14005' and transdate > '31/DEC/2014'; --44 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14006','161030') where GLDEBITACCT like '%-C6MF14006' and transdate > '31/DEC/2014'; --6 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14007','161030') where GLDEBITACCT like '%-C6MF14007' and transdate > '31/DEC/2014'; --20
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14008','161030') where GLDEBITACCT like '%-C6MF14008' and transdate > '31/DEC/2014'; --6 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14009','161030') where GLDEBITACCT like '%-C6MF14009' and transdate > '31/DEC/2014'; --19 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where GLDEBITACCT like '%-C6MF14041' and transdate > '31/DEC/2014'; --31 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14042','161030') where GLDEBITACCT like '%-C6MF14042' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14044','161030') where GLDEBITACCT like '%-C6MF14044' and transdate > '31/DEC/2014'; --6 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14052','161030') where GLDEBITACCT like '%-C6MF14052' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where GLDEBITACCT like '%-C6MF14060' and transdate > '31/DEC/2014'; --4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14061','161030') where GLDEBITACCT like '%-C6MF14061' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14062','161030') where GLDEBITACCT like '%-C6MF14062' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where GLDEBITACCT like '%-C6MF14064' and transdate > '31/DEC/2014'; --39 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14071','161030') where GLDEBITACCT like '%-C6MF14071' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14072','161030') where GLDEBITACCT like '%-C6MF14072' and transdate > '31/DEC/2014'; --5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14082','161030') where GLDEBITACCT like '%-C6MF14082' and transdate > '31/DEC/2014'; --41 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14101','161030') where GLDEBITACCT like '%-C6MF14101' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14110','161030') where GLDEBITACCT like '%-C6MF14110' and transdate > '31/DEC/2014'; --24 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where GLDEBITACCT like '%-C86620301' and transdate > '31/DEC/2014'; --2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86700403','161030') where GLDEBITACCT like '%-C86700403' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where GLDEBITACCT like '%-C86700406' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86700409','161030') where GLDEBITACCT like '%-C86700409' and transdate > '31/DEC/2014'; --24 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86700411','161030') where GLDEBITACCT like '%-C86700411' and transdate > '31/DEC/2014'; --5 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where GLDEBITACCT like '%-C86900303' and transdate > '31/DEC/2014'; --97 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where GLDEBITACCT like '%-C86900401' and transdate > '31/DEC/2014'; --2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where GLDEBITACCT like '%-C86900402' and transdate > '31/DEC/2014'; --4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where GLDEBITACCT like '%-C86900403' and transdate > '31/DEC/2014'; --139 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900404','161030') where GLDEBITACCT like '%-C86900404' and transdate > '31/DEC/2014'; --31 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900407','161030') where GLDEBITACCT like '%-C86900407' and transdate > '31/DEC/2014'; --14 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where GLDEBITACCT like '%-C86900408' and transdate > '31/DEC/2014'; --15 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where GLDEBITACCT like '%-C86900409' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86920401','161030') where GLDEBITACCT like '%-C86920401' and transdate > '31/DEC/2014'; --17 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86920402','161030') where GLDEBITACCT like '%-C86920402' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where GLDEBITACCT like '%-C87510401' and transdate > '31/DEC/2014'; --31 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where GLDEBITACCT like '%-C87510402' and transdate > '31/DEC/2014'; --32 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where GLDEBITACCT like '%-C87510405' and transdate > '31/DEC/2014'; --116 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510407','161040') where GLDEBITACCT like '%-C87510407' and transdate > '31/DEC/2014'; --43 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87515402','161040') where GLDEBITACCT like '%-C87515402' and transdate > '31/DEC/2014'; --2 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87515403','161040') where GLDEBITACCT like '%-C87515403' and transdate > '31/DEC/2014'; --30
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where GLDEBITACCT like '%-C87520304' and transdate > '31/DEC/2014'; --36 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520401','161040') where GLDEBITACCT like '%-C87520401' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where GLDEBITACCT like '%-C87520402' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520404','161060') where GLDEBITACCT like '%-C87520404' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87520407','161040') where GLDEBITACCT like '%-C87520407' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530402','161040') where GLDEBITACCT like '%-C87530402' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where GLDEBITACCT like '%-C87530403' and transdate > '31/DEC/2014'; --16 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where GLDEBITACCT like '%-C87530405' and transdate > '31/DEC/2014'; --19 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87560401','161040') where GLDEBITACCT like '%-C87560401' and transdate > '31/DEC/2014'; --3 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87560402','161040') where GLDEBITACCT like '%-C87560402' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where GLDEBITACCT like '%-C88020301' and transdate > '31/DEC/2014'; --4 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where GLDEBITACCT like '%-C88020302' and transdate > '31/DEC/2014'; --11 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where GLDEBITACCT like '%-C88020401' and transdate > '31/DEC/2014'; --6 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where GLDEBITACCT like '%-C88020402' and transdate > '31/DEC/2014'; --406 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C82060501','161040') where GLDEBITACCT like '%-C82060501' and transdate > '31/DEC/2014'; --10
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86210504','161030') where GLDEBITACCT like '%-C86210504' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900501','161030') where GLDEBITACCT like '%-C86900501' and transdate > '31/DEC/2014'; --15 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where GLDEBITACCT like '%-C86900502' and transdate > '31/DEC/2014'; --70
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87510504','161040') where GLDEBITACCT like '%-C87510504' and transdate > '31/DEC/2014'; --10
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87530507','161040') where GLDEBITACCT like '%-C87530507' and transdate > '31/DEC/2014'; --1 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C87560503','161040') where GLDEBITACCT like '%-C87560503' and transdate > '31/DEC/2014'; --11 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where GLDEBITACCT like '%-C88020501' and transdate > '31/DEC/2014'; --34 rows updated.
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where GLDEBITACCT like '%-C88020509' and transdate > '31/DEC/2014'; --18 rows updated.
commit;
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C23010401','151300') where WOGLACCOUNT like '%-C23010401' and transdate > '31/DEC/2014'; --22 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14000','161030') where WOGLACCOUNT like '%-C6MF14000' and transdate > '31/DEC/2014'; --20
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14001','161030') where WOGLACCOUNT like '%-C6MF14001' and transdate > '31/DEC/2014'; --140
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14002','161030') where WOGLACCOUNT like '%-C6MF14002' and transdate > '31/DEC/2014'; --468 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14004','161030') where WOGLACCOUNT like '%-C6MF14004' and transdate > '31/DEC/2014'; --4 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14005','161030') where WOGLACCOUNT like '%-C6MF14005' and transdate > '31/DEC/2014'; --328 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14006','161030') where WOGLACCOUNT like '%-C6MF14006' and transdate > '31/DEC/2014'; --60
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14007','161030') where WOGLACCOUNT like '%-C6MF14007' and transdate > '31/DEC/2014'; --158 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14008','161030') where WOGLACCOUNT like '%-C6MF14008' and transdate > '31/DEC/2014'; --52 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14009','161030') where WOGLACCOUNT like '%-C6MF14009' and transdate > '31/DEC/2014'; --110
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14041','161030') where WOGLACCOUNT like '%-C6MF14041' and transdate > '31/DEC/2014'; --214 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14042','161030') where WOGLACCOUNT like '%-C6MF14042' and transdate > '31/DEC/2014'; --18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14043','161030') where WOGLACCOUNT like '%-C6MF14043' and transdate > '31/DEC/2014'; --18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14044','161030') where WOGLACCOUNT like '%-C6MF14044' and transdate > '31/DEC/2014'; --28 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14052','161030') where WOGLACCOUNT like '%-C6MF14052' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14060','161030') where WOGLACCOUNT like '%-C6MF14060' and transdate > '31/DEC/2014'; --40
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14061','161030') where WOGLACCOUNT like '%-C6MF14061' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14062','161030') where WOGLACCOUNT like '%-C6MF14062' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14064','161030') where WOGLACCOUNT like '%-C6MF14064' and transdate > '31/DEC/2014'; --260
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14065','161030') where WOGLACCOUNT like '%-C6MF14065' and transdate > '31/DEC/2014'; --18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14071','161030') where WOGLACCOUNT like '%-C6MF14071' and transdate > '31/DEC/2014'; --16 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14072','161030') where WOGLACCOUNT like '%-C6MF14072' and transdate > '31/DEC/2014'; --30
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14082','161030') where WOGLACCOUNT like '%-C6MF14082' and transdate > '31/DEC/2014'; --232 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14101','161030') where WOGLACCOUNT like '%-C6MF14101' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C6MF14110','161030') where WOGLACCOUNT like '%-C6MF14110' and transdate > '31/DEC/2014'; --148 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C77501301','161040') where WOGLACCOUNT like '%-C77501301' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700402','161030') where WOGLACCOUNT like '%-C86700402' and transdate > '31/DEC/2014'; --4 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700403','161030') where WOGLACCOUNT like '%-C86700403' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700404','161030') where WOGLACCOUNT like '%-C86700404' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700406','161030') where WOGLACCOUNT like '%-C86700406' and transdate > '31/DEC/2014'; --14 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700409','161030') where WOGLACCOUNT like '%-C86700409' and transdate > '31/DEC/2014'; --112 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86700411','161030') where WOGLACCOUNT like '%-C86700411' and transdate > '31/DEC/2014'; --24 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900303','161030') where WOGLACCOUNT like '%-C86900303' and transdate > '31/DEC/2014'; --640
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900401','161030') where WOGLACCOUNT like '%-C86900401' and transdate > '31/DEC/2014'; --26 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900402','161030') where WOGLACCOUNT like '%-C86900402' and transdate > '31/DEC/2014'; --50
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900403','161030') where WOGLACCOUNT like '%-C86900403' and transdate > '31/DEC/2014'; --1,030
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900404','161030') where WOGLACCOUNT like '%-C86900404' and transdate > '31/DEC/2014'; --170
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900407','161030') where WOGLACCOUNT like '%-C86900407' and transdate > '31/DEC/2014'; --80
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900408','161030') where WOGLACCOUNT like '%-C86900408' and transdate > '31/DEC/2014'; --228 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900409','161030') where WOGLACCOUNT like '%-C86900409' and transdate > '31/DEC/2014'; --46 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86920401','161030') where WOGLACCOUNT like '%-C86920401' and transdate > '31/DEC/2014'; --90
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86920402','161030') where WOGLACCOUNT like '%-C86920402' and transdate > '31/DEC/2014'; --18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87510401','161040') where WOGLACCOUNT like '%-C87510401' and transdate > '31/DEC/2014'; --122 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87510402','161040') where WOGLACCOUNT like '%-C87510402' and transdate > '31/DEC/2014'; --128 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87510405','161040') where WOGLACCOUNT like '%-C87510405' and transdate > '31/DEC/2014'; --806 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87510407','161040') where WOGLACCOUNT like '%-C87510407' and transdate > '31/DEC/2014'; --200
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87515402','161040') where WOGLACCOUNT like '%-C87515402' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87515403','161040') where WOGLACCOUNT like '%-C87515403' and transdate > '31/DEC/2014'; --150
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87520304','161040') where WOGLACCOUNT like '%-C87520304' and transdate > '31/DEC/2014'; --132 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87520401','161040') where WOGLACCOUNT like '%-C87520401' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87520402','161040') where WOGLACCOUNT like '%-C87520402' and transdate > '31/DEC/2014'; --32 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87520404','161060') where WOGLACCOUNT like '%-C87520404' and transdate > '31/DEC/2014'; --14 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87520407','161040') where WOGLACCOUNT like '%-C87520407' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87530402','161040') where WOGLACCOUNT like '%-C87530402' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87530403','161040') where WOGLACCOUNT like '%-C87530403' and transdate > '31/DEC/2014'; --62 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87530405','161040') where WOGLACCOUNT like '%-C87530405' and transdate > '31/DEC/2014'; --72 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87530504','161040') where WOGLACCOUNT like '%-C87530504' and transdate > '31/DEC/2014'; --18 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87560303','161040') where WOGLACCOUNT like '%-C87560303' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87560401','161040') where WOGLACCOUNT like '%-C87560401' and transdate > '31/DEC/2014'; --30
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87560402','161040') where WOGLACCOUNT like '%-C87560402' and transdate > '31/DEC/2014'; --10
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87560404','161040') where WOGLACCOUNT like '%-C87560404' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020301','161042') where WOGLACCOUNT like '%-C88020301' and transdate > '31/DEC/2014'; --22 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020302','161040') where WOGLACCOUNT like '%-C88020302' and transdate > '31/DEC/2014'; --72 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020401','161042') where WOGLACCOUNT like '%-C88020401' and transdate > '31/DEC/2014'; --28 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020402','161042') where WOGLACCOUNT like '%-C88020402' and transdate > '31/DEC/2014'; --1,822 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C82060501','161040') where WOGLACCOUNT like '%-C82060501' and transdate > '31/DEC/2014'; --46 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900501','161030') where WOGLACCOUNT like '%-C86900501' and transdate > '31/DEC/2014'; --82 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86900502','161030') where WOGLACCOUNT like '%-C86900502' and transdate > '31/DEC/2014'; --460
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87510504','161040') where WOGLACCOUNT like '%-C87510504' and transdate > '31/DEC/2014'; --52 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87530507','161040') where WOGLACCOUNT like '%-C87530507' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C87560503','161040') where WOGLACCOUNT like '%-C87560503' and transdate > '31/DEC/2014'; --58 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020501','161042') where WOGLACCOUNT like '%-C88020501' and transdate > '31/DEC/2014'; --76 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020509','161042') where WOGLACCOUNT like '%-C88020509' and transdate > '31/DEC/2014'; --122 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C23010401','151300') where GLDEBITACCT like '%-C23010401' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14000','161030') where GLDEBITACCT like '%-C6MF14000' and transdate > '31/DEC/2014'; --4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14001','161030') where GLDEBITACCT like '%-C6MF14001' and transdate > '31/DEC/2014'; --40
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where GLDEBITACCT like '%-C6MF14002' and transdate > '31/DEC/2014'; --124 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where GLDEBITACCT like '%-C6MF14005' and transdate > '31/DEC/2014'; --106 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14006','161030') where GLDEBITACCT like '%-C6MF14006' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14007','161030') where GLDEBITACCT like '%-C6MF14007' and transdate > '31/DEC/2014'; --40
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14008','161030') where GLDEBITACCT like '%-C6MF14008' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14009','161030') where GLDEBITACCT like '%-C6MF14009' and transdate > '31/DEC/2014'; --38 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where GLDEBITACCT like '%-C6MF14041' and transdate > '31/DEC/2014'; --66 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14042','161030') where GLDEBITACCT like '%-C6MF14042' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14043','161030') where GLDEBITACCT like '%-C6MF14043' and transdate > '31/DEC/2014'; --8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14044','161030') where GLDEBITACCT like '%-C6MF14044' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14052','161030') where GLDEBITACCT like '%-C6MF14052' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where GLDEBITACCT like '%-C6MF14060' and transdate > '31/DEC/2014'; --16 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14061','161030') where GLDEBITACCT like '%-C6MF14061' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14062','161030') where GLDEBITACCT like '%-C6MF14062' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where GLDEBITACCT like '%-C6MF14064' and transdate > '31/DEC/2014'; --80
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14065','161030') where GLDEBITACCT like '%-C6MF14065' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14071','161030') where GLDEBITACCT like '%-C6MF14071' and transdate > '31/DEC/2014'; --8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14072','161030') where GLDEBITACCT like '%-C6MF14072' and transdate > '31/DEC/2014'; --10
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14082','161030') where GLDEBITACCT like '%-C6MF14082' and transdate > '31/DEC/2014'; --82 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14101','161030') where GLDEBITACCT like '%-C6MF14101' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14110','161030') where GLDEBITACCT like '%-C6MF14110' and transdate > '31/DEC/2014'; --48 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where GLDEBITACCT like '%-C86620301' and transdate > '31/DEC/2014'; --4 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86700403','161030') where GLDEBITACCT like '%-C86700403' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where GLDEBITACCT like '%-C86700406' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86700409','161030') where GLDEBITACCT like '%-C86700409' and transdate > '31/DEC/2014'; --46 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86700411','161030') where GLDEBITACCT like '%-C86700411' and transdate > '31/DEC/2014'; --10
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where GLDEBITACCT like '%-C86900303' and transdate > '31/DEC/2014'; --216 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where GLDEBITACCT like '%-C86900401' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where GLDEBITACCT like '%-C86900402' and transdate > '31/DEC/2014'; --10
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where GLDEBITACCT like '%-C86900403' and transdate > '31/DEC/2014'; --338 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900404','161030') where GLDEBITACCT like '%-C86900404' and transdate > '31/DEC/2014'; --62 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900407','161030') where GLDEBITACCT like '%-C86900407' and transdate > '31/DEC/2014'; --28 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where GLDEBITACCT like '%-C86900408' and transdate > '31/DEC/2014'; --76 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where GLDEBITACCT like '%-C86900409' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86920401','161030') where GLDEBITACCT like '%-C86920401' and transdate > '31/DEC/2014'; --34 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86920402','161030') where GLDEBITACCT like '%-C86920402' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where GLDEBITACCT like '%-C87510401' and transdate > '31/DEC/2014'; --62 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where GLDEBITACCT like '%-C87510402' and transdate > '31/DEC/2014'; --64 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where GLDEBITACCT like '%-C87510405' and transdate > '31/DEC/2014'; --232 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87510407','161040') where GLDEBITACCT like '%-C87510407' and transdate > '31/DEC/2014'; --86 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87515402','161040') where GLDEBITACCT like '%-C87515402' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87515403','161040') where GLDEBITACCT like '%-C87515403' and transdate > '31/DEC/2014'; --66 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where GLDEBITACCT like '%-C87520304' and transdate > '31/DEC/2014'; --76 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87520401','161040') where GLDEBITACCT like '%-C87520401' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where GLDEBITACCT like '%-C87520402' and transdate > '31/DEC/2014'; --8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87520404','161060') where GLDEBITACCT like '%-C87520404' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87520407','161040') where GLDEBITACCT like '%-C87520407' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87530402','161040') where GLDEBITACCT like '%-C87530402' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where GLDEBITACCT like '%-C87530403' and transdate > '31/DEC/2014'; --32 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where GLDEBITACCT like '%-C87530405' and transdate > '31/DEC/2014'; --38 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87530504','161040') where GLDEBITACCT like '%-C87530504' and transdate > '31/DEC/2014'; --8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87560401','161040') where GLDEBITACCT like '%-C87560401' and transdate > '31/DEC/2014'; --6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87560402','161040') where GLDEBITACCT like '%-C87560402' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where GLDEBITACCT like '%-C87560404' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where GLDEBITACCT like '%-C88020301' and transdate > '31/DEC/2014'; --8 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where GLDEBITACCT like '%-C88020302' and transdate > '31/DEC/2014'; --26 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where GLDEBITACCT like '%-C88020401' and transdate > '31/DEC/2014'; --12 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where GLDEBITACCT like '%-C88020402' and transdate > '31/DEC/2014'; --884 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C82060501','161040') where GLDEBITACCT like '%-C82060501' and transdate > '31/DEC/2014'; --20
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86210504','161030') where GLDEBITACCT like '%-C86210504' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900501','161030') where GLDEBITACCT like '%-C86900501' and transdate > '31/DEC/2014'; --30
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where GLDEBITACCT like '%-C86900502' and transdate > '31/DEC/2014'; --174 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87510504','161040') where GLDEBITACCT like '%-C87510504' and transdate > '31/DEC/2014'; --24 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87530507','161040') where GLDEBITACCT like '%-C87530507' and transdate > '31/DEC/2014'; --2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C87560503','161040') where GLDEBITACCT like '%-C87560503' and transdate > '31/DEC/2014'; --22 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where GLDEBITACCT like '%-C88020501' and transdate > '31/DEC/2014'; --80
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where GLDEBITACCT like '%-C88020509' and transdate > '31/DEC/2014'; --80
commit;
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102109','161060') where GLACCOUNT like '%-C20102109'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102113','161040') where GLACCOUNT like '%-C20102113'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102114','161040') where GLACCOUNT like '%-C20102114'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102115','161040') where GLACCOUNT like '%-C20102115'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102118','161040') where GLACCOUNT like '%-C20102118'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20102123','161040') where GLACCOUNT like '%-C20102123'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C20609001','161040') where GLACCOUNT like '%-C20609001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C22060302','151300') where GLACCOUNT like '%-C22060302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C23010401','151300') where GLACCOUNT like '%-C23010401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C25MAXIMO','151300') where GLACCOUNT like '%-C25MAXIMO'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30101001','161070') where GLACCOUNT like '%-C30101001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30101007','161070') where GLACCOUNT like '%-C30101007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30101009','161060') where GLACCOUNT like '%-C30101009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151001','161060') where GLACCOUNT like '%-C30151001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151002','161060') where GLACCOUNT like '%-C30151002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151004','161060') where GLACCOUNT like '%-C30151004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151005','161060') where GLACCOUNT like '%-C30151005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151006','161060') where GLACCOUNT like '%-C30151006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151007','161060') where GLACCOUNT like '%-C30151007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151008','161060') where GLACCOUNT like '%-C30151008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151010','161060') where GLACCOUNT like '%-C30151010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151011','161060') where GLACCOUNT like '%-C30151011'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151013','161060') where GLACCOUNT like '%-C30151013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151020','161060') where GLACCOUNT like '%-C30151020'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30151021','161060') where GLACCOUNT like '%-C30151021'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159001','161060') where GLACCOUNT like '%-C30159001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159002','161060') where GLACCOUNT like '%-C30159002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159003','161060') where GLACCOUNT like '%-C30159003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159004','161060') where GLACCOUNT like '%-C30159004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159005','161060') where GLACCOUNT like '%-C30159005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159006','161060') where GLACCOUNT like '%-C30159006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159007','161060') where GLACCOUNT like '%-C30159007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159008','151300') where GLACCOUNT like '%-C30159008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C30159009','161060') where GLACCOUNT like '%-C30159009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C55009001','161070') where GLACCOUNT like '%-C55009001'; --2 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C55CRANE8','161070') where GLACCOUNT like '%-C55CRANE8'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C55SECUR7','161060') where GLACCOUNT like '%-C55SECUR7'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG0902','161030') where GLACCOUNT like '%-C6ENG0902'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG0903','161030') where GLACCOUNT like '%-C6ENG0903'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG0904','161030') where GLACCOUNT like '%-C6ENG0904'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1001','161030') where GLACCOUNT like '%-C6ENG1001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1002','161030') where GLACCOUNT like '%-C6ENG1002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1003','161030') where GLACCOUNT like '%-C6ENG1003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1004','161030') where GLACCOUNT like '%-C6ENG1004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1005','161030') where GLACCOUNT like '%-C6ENG1005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1008','161030') where GLACCOUNT like '%-C6ENG1008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6ENG1009','161032') where GLACCOUNT like '%-C6ENG1009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS09001','161030') where GLACCOUNT like '%-C6FS09001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS09002','161030') where GLACCOUNT like '%-C6FS09002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS09003','161030') where GLACCOUNT like '%-C6FS09003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS10001','161030') where GLACCOUNT like '%-C6FS10001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS10002','161030') where GLACCOUNT like '%-C6FS10002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6FS10003','161030') where GLACCOUNT like '%-C6FS10003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14000','161030') where GLACCOUNT like '%-C6MF14000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14001','161030') where GLACCOUNT like '%-C6MF14001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14002','161030') where GLACCOUNT like '%-C6MF14002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14003','161030') where GLACCOUNT like '%-C6MF14003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14004','161030') where GLACCOUNT like '%-C6MF14004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14005','161030') where GLACCOUNT like '%-C6MF14005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14006','161030') where GLACCOUNT like '%-C6MF14006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14007','161030') where GLACCOUNT like '%-C6MF14007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14008','161030') where GLACCOUNT like '%-C6MF14008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14009','161030') where GLACCOUNT like '%-C6MF14009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14020','161030') where GLACCOUNT like '%-C6MF14020'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14021','161030') where GLACCOUNT like '%-C6MF14021'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14022','161030') where GLACCOUNT like '%-C6MF14022'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14040','161030') where GLACCOUNT like '%-C6MF14040'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14041','161030') where GLACCOUNT like '%-C6MF14041'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14042','161030') where GLACCOUNT like '%-C6MF14042'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14043','161030') where GLACCOUNT like '%-C6MF14043'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14044','161030') where GLACCOUNT like '%-C6MF14044'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14050','161030') where GLACCOUNT like '%-C6MF14050'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14052','161030') where GLACCOUNT like '%-C6MF14052'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14054','161030') where GLACCOUNT like '%-C6MF14054'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14060','161030') where GLACCOUNT like '%-C6MF14060'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14061','161030') where GLACCOUNT like '%-C6MF14061'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14062','161030') where GLACCOUNT like '%-C6MF14062'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14063','161030') where GLACCOUNT like '%-C6MF14063'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14064','161030') where GLACCOUNT like '%-C6MF14064'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14065','161030') where GLACCOUNT like '%-C6MF14065'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14066','161030') where GLACCOUNT like '%-C6MF14066'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14067','161030') where GLACCOUNT like '%-C6MF14067'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14070','161030') where GLACCOUNT like '%-C6MF14070'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14071','161030') where GLACCOUNT like '%-C6MF14071'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14072','161030') where GLACCOUNT like '%-C6MF14072'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14073','161030') where GLACCOUNT like '%-C6MF14073'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14082','161030') where GLACCOUNT like '%-C6MF14082'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14101','161030') where GLACCOUNT like '%-C6MF14101'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14110','161030') where GLACCOUNT like '%-C6MF14110'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0721','161030') where GLACCOUNT like '%-C6MIN0721'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0727','161030') where GLACCOUNT like '%-C6MIN0727'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0806','161030') where GLACCOUNT like '%-C6MIN0806'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0809','161030') where GLACCOUNT like '%-C6MIN0809'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0816','161030') where GLACCOUNT like '%-C6MIN0816'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0818','161030') where GLACCOUNT like '%-C6MIN0818'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0822','161030') where GLACCOUNT like '%-C6MIN0822'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0825','161030') where GLACCOUNT like '%-C6MIN0825'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0826','161030') where GLACCOUNT like '%-C6MIN0826'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0827','161030') where GLACCOUNT like '%-C6MIN0827'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MIN0829','161030') where GLACCOUNT like '%-C6MIN0829'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MINT1031','161030') where GLACCOUNT like '%-C6MINT1031'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0901','161030') where GLACCOUNT like '%-C6MNT0901'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0903','161030') where GLACCOUNT like '%-C6MNT0903'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0905','161030') where GLACCOUNT like '%-C6MNT0905'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0906','161030') where GLACCOUNT like '%-C6MNT0906'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0908','161030') where GLACCOUNT like '%-C6MNT0908'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0909','161030') where GLACCOUNT like '%-C6MNT0909'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT0918','161030') where GLACCOUNT like '%-C6MNT0918'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1005','161030') where GLACCOUNT like '%-C6MNT1005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1008','161030') where GLACCOUNT like '%-C6MNT1008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1013','161030') where GLACCOUNT like '%-C6MNT1013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1014','161030') where GLACCOUNT like '%-C6MNT1014'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1017','161030') where GLACCOUNT like '%-C6MNT1017'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1031','161030') where GLACCOUNT like '%-C6MNT1031'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1035','161030') where GLACCOUNT like '%-C6MNT1035'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1036','161030') where GLACCOUNT like '%-C6MNT1036'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1037','161030') where GLACCOUNT like '%-C6MNT1037'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1038','161030') where GLACCOUNT like '%-C6MNT1038'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1039','161030') where GLACCOUNT like '%-C6MNT1039'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1112','161030') where GLACCOUNT like '%-C6MNT1112'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1123','161030') where GLACCOUNT like '%-C6MNT1123'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1205','161030') where GLACCOUNT like '%-C6MNT1205'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MNT1213','161030') where GLACCOUNT like '%-C6MNT1213'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6NORTH14','161032') where GLACCOUNT like '%-C6NORTH14'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OBREM01','161032') where GLACCOUNT like '%-C6OBREM01'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0901','161032') where GLACCOUNT like '%-C6OPS0901'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0902','161030') where GLACCOUNT like '%-C6OPS0902'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0903','161030') where GLACCOUNT like '%-C6OPS0903'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0904','161030') where GLACCOUNT like '%-C6OPS0904'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0905','161030') where GLACCOUNT like '%-C6OPS0905'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS0906','161030') where GLACCOUNT like '%-C6OPS0906'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1001','161030') where GLACCOUNT like '%-C6OPS1001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1004','161030') where GLACCOUNT like '%-C6OPS1004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1007','161030') where GLACCOUNT like '%-C6OPS1007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1008','161030') where GLACCOUNT like '%-C6OPS1008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1009','161030') where GLACCOUNT like '%-C6OPS1009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1010','161030') where GLACCOUNT like '%-C6OPS1010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1201','161030') where GLACCOUNT like '%-C6OPS1201'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6OPS1202','161030') where GLACCOUNT like '%-C6OPS1202'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0517','161030') where GLACCOUNT like '%-C6PLT0517'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0801','161030') where GLACCOUNT like '%-C6PLT0801'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0802','161030') where GLACCOUNT like '%-C6PLT0802'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0804','161030') where GLACCOUNT like '%-C6PLT0804'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0806','161030') where GLACCOUNT like '%-C6PLT0806'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0811','161030') where GLACCOUNT like '%-C6PLT0811'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6PLT0819','161030') where GLACCOUNT like '%-C6PLT0819'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6TE10001','161030') where GLACCOUNT like '%-C6TE10001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W312111','161032') where GLACCOUNT like '%-C6W312111'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W312230','161032') where GLACCOUNT like '%-C6W312230'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W354210','161032') where GLACCOUNT like '%-C6W354210'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W362400','161032') where GLACCOUNT like '%-C6W362400'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W475300','161032') where GLACCOUNT like '%-C6W475300'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W654115','161032') where GLACCOUNT like '%-C6W654115'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W654120','161032') where GLACCOUNT like '%-C6W654120'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675110','161032') where GLACCOUNT like '%-C6W675110'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675140','161032') where GLACCOUNT like '%-C6W675140'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675320','161032') where GLACCOUNT like '%-C6W675320'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675340','161032') where GLACCOUNT like '%-C6W675340'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675350','161032') where GLACCOUNT like '%-C6W675350'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675360','161032') where GLACCOUNT like '%-C6W675360'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675361','161032') where GLACCOUNT like '%-C6W675361'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675362','161032') where GLACCOUNT like '%-C6W675362'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675390','161032') where GLACCOUNT like '%-C6W675390'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675400','161032') where GLACCOUNT like '%-C6W675400'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675500','161032') where GLACCOUNT like '%-C6W675500'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W675550','161032') where GLACCOUNT like '%-C6W675550'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W792001','161032') where GLACCOUNT like '%-C6W792001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W792002','161032') where GLACCOUNT like '%-C6W792002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W792003','161032') where GLACCOUNT like '%-C6W792003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W854110','161032') where GLACCOUNT like '%-C6W854110'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W854111','161032') where GLACCOUNT like '%-C6W854111'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W954110','161032') where GLACCOUNT like '%-C6W954110'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6W954111','161032') where GLACCOUNT like '%-C6W954111'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010003','161040') where GLACCOUNT like '%-C72010003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010005','161040') where GLACCOUNT like '%-C72010005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010009','161040') where GLACCOUNT like '%-C72010009'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010010','161040') where GLACCOUNT like '%-C72010010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010011','161040') where GLACCOUNT like '%-C72010011'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010012','161040') where GLACCOUNT like '%-C72010012'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010013','161040') where GLACCOUNT like '%-C72010013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010015','161040') where GLACCOUNT like '%-C72010015'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010016','161040') where GLACCOUNT like '%-C72010016'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010017','161040') where GLACCOUNT like '%-C72010017'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010022','161040') where GLACCOUNT like '%-C72010022'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010023','161040') where GLACCOUNT like '%-C72010023'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010024','161040') where GLACCOUNT like '%-C72010024'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010027','161040') where GLACCOUNT like '%-C72010027'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010028','161040') where GLACCOUNT like '%-C72010028'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010029','161040') where GLACCOUNT like '%-C72010029'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010030','161040') where GLACCOUNT like '%-C72010030'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010031','161040') where GLACCOUNT like '%-C72010031'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010034','161040') where GLACCOUNT like '%-C72010034'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010035','161040') where GLACCOUNT like '%-C72010035'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010036','161040') where GLACCOUNT like '%-C72010036'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010102','161040') where GLACCOUNT like '%-C72010102'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010103','161040') where GLACCOUNT like '%-C72010103'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010104','161040') where GLACCOUNT like '%-C72010104'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010105','161040') where GLACCOUNT like '%-C72010105'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010106','161040') where GLACCOUNT like '%-C72010106'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010108','161040') where GLACCOUNT like '%-C72010108'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010112','161040') where GLACCOUNT like '%-C72010112'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010113','161040') where GLACCOUNT like '%-C72010113'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010114','161040') where GLACCOUNT like '%-C72010114'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010115','161040') where GLACCOUNT like '%-C72010115'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010116','161040') where GLACCOUNT like '%-C72010116'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010117','161040') where GLACCOUNT like '%-C72010117'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010118','161040') where GLACCOUNT like '%-C72010118'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010119','161040') where GLACCOUNT like '%-C72010119'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010121','161040') where GLACCOUNT like '%-C72010121'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010122','161040') where GLACCOUNT like '%-C72010122'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010123','161040') where GLACCOUNT like '%-C72010123'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72010124','161040') where GLACCOUNT like '%-C72010124'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011006','161040') where GLACCOUNT like '%-C72011006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011007','161040') where GLACCOUNT like '%-C72011007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011010','161040') where GLACCOUNT like '%-C72011010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011023','161040') where GLACCOUNT like '%-C72011023'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011033','161040') where GLACCOUNT like '%-C72011033'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011102','161040') where GLACCOUNT like '%-C72011102'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011103','161040') where GLACCOUNT like '%-C72011103'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011104','161040') where GLACCOUNT like '%-C72011104'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011105','161040') where GLACCOUNT like '%-C72011105'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011106','161040') where GLACCOUNT like '%-C72011106'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011108','161040') where GLACCOUNT like '%-C72011108'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72011109','161040') where GLACCOUNT like '%-C72011109'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72017005','161040') where GLACCOUNT like '%-C72017005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72017013','161040') where GLACCOUNT like '%-C72017013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018001','161040') where GLACCOUNT like '%-C72018001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018003','161040') where GLACCOUNT like '%-C72018003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018004','161040') where GLACCOUNT like '%-C72018004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018013','161040') where GLACCOUNT like '%-C72018013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018022','161040') where GLACCOUNT like '%-C72018022'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018023','161040') where GLACCOUNT like '%-C72018023'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018024','161040') where GLACCOUNT like '%-C72018024'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018025','161040') where GLACCOUNT like '%-C72018025'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72018026','161040') where GLACCOUNT like '%-C72018026'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019004','161040') where GLACCOUNT like '%-C72019004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019006','161040') where GLACCOUNT like '%-C72019006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019007','161040') where GLACCOUNT like '%-C72019007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019008','161040') where GLACCOUNT like '%-C72019008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019010','161040') where GLACCOUNT like '%-C72019010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019011','161040') where GLACCOUNT like '%-C72019011'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019012','161040') where GLACCOUNT like '%-C72019012'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019013','161040') where GLACCOUNT like '%-C72019013'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019014','161040') where GLACCOUNT like '%-C72019014'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019015','161040') where GLACCOUNT like '%-C72019015'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019017','161040') where GLACCOUNT like '%-C72019017'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019018','161040') where GLACCOUNT like '%-C72019018'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019019','161040') where GLACCOUNT like '%-C72019019'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019020','161040') where GLACCOUNT like '%-C72019020'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019022','161040') where GLACCOUNT like '%-C72019022'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019023','161040') where GLACCOUNT like '%-C72019023'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019027','161040') where GLACCOUNT like '%-C72019027'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019028','161040') where GLACCOUNT like '%-C72019028'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019029','161040') where GLACCOUNT like '%-C72019029'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019101','161040') where GLACCOUNT like '%-C72019101'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019102','161040') where GLACCOUNT like '%-C72019102'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019103','161040') where GLACCOUNT like '%-C72019103'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019104','161040') where GLACCOUNT like '%-C72019104'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019105','161040') where GLACCOUNT like '%-C72019105'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019106','161040') where GLACCOUNT like '%-C72019106'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019107','161040') where GLACCOUNT like '%-C72019107'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019108','161040') where GLACCOUNT like '%-C72019108'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019109','161040') where GLACCOUNT like '%-C72019109'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019110','161040') where GLACCOUNT like '%-C72019110'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019111','161040') where GLACCOUNT like '%-C72019111'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019112','161040') where GLACCOUNT like '%-C72019112'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019113','161040') where GLACCOUNT like '%-C72019113'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019114','161040') where GLACCOUNT like '%-C72019114'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019116','161040') where GLACCOUNT like '%-C72019116'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019118','161040') where GLACCOUNT like '%-C72019118'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019119','161040') where GLACCOUNT like '%-C72019119'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72019120','161040') where GLACCOUNT like '%-C72019120'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C72058011','161040') where GLACCOUNT like '%-C72058011'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75010201','161042') where GLACCOUNT like '%-C75010201'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75010202','161040') where GLACCOUNT like '%-C75010202'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75010203','161042') where GLACCOUNT like '%-C75010203'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75011201','161042') where GLACCOUNT like '%-C75011201'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75012202','161042') where GLACCOUNT like '%-C75012202'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75018603','161042') where GLACCOUNT like '%-C75018603'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75019202','161042') where GLACCOUNT like '%-C75019202'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75019203','161042') where GLACCOUNT like '%-C75019203'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100301','161040') where GLACCOUNT like '%-C75100301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100302','161040') where GLACCOUNT like '%-C75100302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100303','161040') where GLACCOUNT like '%-C75100303'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100304','161040') where GLACCOUNT like '%-C75100304'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100305','161040') where GLACCOUNT like '%-C75100305'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100306','161040') where GLACCOUNT like '%-C75100306'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100307','161040') where GLACCOUNT like '%-C75100307'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100308','161040') where GLACCOUNT like '%-C75100308'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100309','161040') where GLACCOUNT like '%-C75100309'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100310','161040') where GLACCOUNT like '%-C75100310'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100311','161040') where GLACCOUNT like '%-C75100311'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100312','161040') where GLACCOUNT like '%-C75100312'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100314','161040') where GLACCOUNT like '%-C75100314'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100315','161040') where GLACCOUNT like '%-C75100315'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75100316','161040') where GLACCOUNT like '%-C75100316'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75101301','161040') where GLACCOUNT like '%-C75101301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75102302','161040') where GLACCOUNT like '%-C75102302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75102305','161040') where GLACCOUNT like '%-C75102305'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75109302','161040') where GLACCOUNT like '%-C75109302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75109306','161040') where GLACCOUNT like '%-C75109306'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75109309','161040') where GLACCOUNT like '%-C75109309'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75109310','161040') where GLACCOUNT like '%-C75109310'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75150401','161040') where GLACCOUNT like '%-C75150401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75150402','161040') where GLACCOUNT like '%-C75150402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75150403','161040') where GLACCOUNT like '%-C75150403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75150404','161040') where GLACCOUNT like '%-C75150404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75151402','161040') where GLACCOUNT like '%-C75151402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75158501','161040') where GLACCOUNT like '%-C75158501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75159401','161040') where GLACCOUNT like '%-C75159401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75159402','161040') where GLACCOUNT like '%-C75159402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75159403','161040') where GLACCOUNT like '%-C75159403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200501','161040') where GLACCOUNT like '%-C75200501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200502','161040') where GLACCOUNT like '%-C75200502'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200504','161040') where GLACCOUNT like '%-C75200504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200505','161040') where GLACCOUNT like '%-C75200505'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200508','161040') where GLACCOUNT like '%-C75200508'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75200513','161040') where GLACCOUNT like '%-C75200513'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75201507','161040') where GLACCOUNT like '%-C75201507'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75202501','161040') where GLACCOUNT like '%-C75202501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75202504','161040') where GLACCOUNT like '%-C75202504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75208301','161040') where GLACCOUNT like '%-C75208301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75209502','161040') where GLACCOUNT like '%-C75209502'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75209503','161040') where GLACCOUNT like '%-C75209503'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75209504','161040') where GLACCOUNT like '%-C75209504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75209505','161040') where GLACCOUNT like '%-C75209505'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75209507','161040') where GLACCOUNT like '%-C75209507'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300601','161040') where GLACCOUNT like '%-C75300601'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300602','161040') where GLACCOUNT like '%-C75300602'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300603','161040') where GLACCOUNT like '%-C75300603'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300605','161040') where GLACCOUNT like '%-C75300605'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300611','161040') where GLACCOUNT like '%-C75300611'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75300612','161040') where GLACCOUNT like '%-C75300612'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75308401','161040') where GLACCOUNT like '%-C75308401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75309601','161040') where GLACCOUNT like '%-C75309601'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75600701','161040') where GLACCOUNT like '%-C75600701'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75600703','161040') where GLACCOUNT like '%-C75600703'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75600707','161040') where GLACCOUNT like '%-C75600707'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75601704','161040') where GLACCOUNT like '%-C75601704'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75608702','161040') where GLACCOUNT like '%-C75608702'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75608703','161040') where GLACCOUNT like '%-C75608703'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75608704','161040') where GLACCOUNT like '%-C75608704'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75609701','161040') where GLACCOUNT like '%-C75609701'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C75609702','161040') where GLACCOUNT like '%-C75609702'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C76357222','161040') where GLACCOUNT like '%-C76357222'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C76358222','161040') where GLACCOUNT like '%-C76358222'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C76407231','161040') where GLACCOUNT like '%-C76407231'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C77017804','161040') where GLACCOUNT like '%-C77017804'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C77017805','161040') where GLACCOUNT like '%-C77017805'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C77017807','161040') where GLACCOUNT like '%-C77017807'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C77501301','161040') where GLACCOUNT like '%-C77501301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86210302','161030') where GLACCOUNT like '%-C86210302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610302','161030') where GLACCOUNT like '%-C86610302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610303','161030') where GLACCOUNT like '%-C86610303'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610305','161030') where GLACCOUNT like '%-C86610305'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610306','161030') where GLACCOUNT like '%-C86610306'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610307','161030') where GLACCOUNT like '%-C86610307'; --2 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610309','161030') where GLACCOUNT like '%-C86610309'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C866106306','161030') where GLACCOUNT like '%-C866106306'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86610804','161030') where GLACCOUNT like '%-C86610804'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86620301','161030') where GLACCOUNT like '%-C86620301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700401','161030') where GLACCOUNT like '%-C86700401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700402','161030') where GLACCOUNT like '%-C86700402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700403','161030') where GLACCOUNT like '%-C86700403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700404','161030') where GLACCOUNT like '%-C86700404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700405','161030') where GLACCOUNT like '%-C86700405'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700406','161030') where GLACCOUNT like '%-C86700406'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700409','161030') where GLACCOUNT like '%-C86700409'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700410','161030') where GLACCOUNT like '%-C86700410'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86700411','161030') where GLACCOUNT like '%-C86700411'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900301','161030') where GLACCOUNT like '%-C86900301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900302','161030') where GLACCOUNT like '%-C86900302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900303','161030') where GLACCOUNT like '%-C86900303'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900401','161030') where GLACCOUNT like '%-C86900401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900402','161030') where GLACCOUNT like '%-C86900402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900403','161030') where GLACCOUNT like '%-C86900403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900404','161030') where GLACCOUNT like '%-C86900404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900407','161030') where GLACCOUNT like '%-C86900407'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900408','161030') where GLACCOUNT like '%-C86900408'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900409','161030') where GLACCOUNT like '%-C86900409'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920301','161030') where GLACCOUNT like '%-C86920301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920304','161030') where GLACCOUNT like '%-C86920304'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920306','161030') where GLACCOUNT like '%-C86920306'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920401','161030') where GLACCOUNT like '%-C86920401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920402','161030') where GLACCOUNT like '%-C86920402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86MF14002','161030') where GLACCOUNT like '%-C86MF14002'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86MF14003','161030') where GLACCOUNT like '%-C86MF14003'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510301','161040') where GLACCOUNT like '%-C87510301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510304','161040') where GLACCOUNT like '%-C87510304'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510305','161040') where GLACCOUNT like '%-C87510305'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510306','161040') where GLACCOUNT like '%-C87510306'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510307','161040') where GLACCOUNT like '%-C87510307'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510401','161040') where GLACCOUNT like '%-C87510401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510402','161040') where GLACCOUNT like '%-C87510402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510405','161040') where GLACCOUNT like '%-C87510405'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510407','161040') where GLACCOUNT like '%-C87510407'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87515402','161040') where GLACCOUNT like '%-C87515402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87515403','161040') where GLACCOUNT like '%-C87515403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520302','161040') where GLACCOUNT like '%-C87520302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520303','161040') where GLACCOUNT like '%-C87520303'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520304','161040') where GLACCOUNT like '%-C87520304'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520305','161040') where GLACCOUNT like '%-C87520305'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520401','161040') where GLACCOUNT like '%-C87520401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520402','161040') where GLACCOUNT like '%-C87520402'; --2 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520404','161060') where GLACCOUNT like '%-C87520404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520405','161060') where GLACCOUNT like '%-C87520405'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520406','161040') where GLACCOUNT like '%-C87520406'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520407','161040') where GLACCOUNT like '%-C87520407'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530301','161040') where GLACCOUNT like '%-C87530301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530402','161040') where GLACCOUNT like '%-C87530402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530403','161040') where GLACCOUNT like '%-C87530403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530404','161040') where GLACCOUNT like '%-C87530404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530405','161040') where GLACCOUNT like '%-C87530405'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530504','161040') where GLACCOUNT like '%-C87530504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C875330402','161040') where GLACCOUNT like '%-C875330402'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560301','161040') where GLACCOUNT like '%-C87560301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560302','161040') where GLACCOUNT like '%-C87560302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560303','161040') where GLACCOUNT like '%-C87560303'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560401','161040') where GLACCOUNT like '%-C87560401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560402','161040') where GLACCOUNT like '%-C87560402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560403','161040') where GLACCOUNT like '%-C87560403'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560404','161040') where GLACCOUNT like '%-C87560404'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560502','161040') where GLACCOUNT like '%-C87560502'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020301','161042') where GLACCOUNT like '%-C88020301'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020302','161040') where GLACCOUNT like '%-C88020302'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020401','161042') where GLACCOUNT like '%-C88020401'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020402','161042') where GLACCOUNT like '%-C88020402'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90111010','161040') where GLACCOUNT like '%-C90111010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90112000','161040') where GLACCOUNT like '%-C90112000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90121010','161040') where GLACCOUNT like '%-C90121010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90131010','161040') where GLACCOUNT like '%-C90131010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90131020','161040') where GLACCOUNT like '%-C90131020'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90131030','161040') where GLACCOUNT like '%-C90131030'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90211000','161040') where GLACCOUNT like '%-C90211000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90212000','161040') where GLACCOUNT like '%-C90212000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90221000','161040') where GLACCOUNT like '%-C90221000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90222000','161040') where GLACCOUNT like '%-C90222000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90311000','161040') where GLACCOUNT like '%-C90311000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90312000','161040') where GLACCOUNT like '%-C90312000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90322000','161040') where GLACCOUNT like '%-C90322000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90411010','161040') where GLACCOUNT like '%-C90411010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90411030','161040') where GLACCOUNT like '%-C90411030'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90412000','161040') where GLACCOUNT like '%-C90412000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90511010','161040') where GLACCOUNT like '%-C90511010'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90511020','161040') where GLACCOUNT like '%-C90511020'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90511040','161040') where GLACCOUNT like '%-C90511040'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90513000','161040') where GLACCOUNT like '%-C90513000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90611000','161040') where GLACCOUNT like '%-C90611000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90612000','161040') where GLACCOUNT like '%-C90612000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90613001','161040') where GLACCOUNT like '%-C90613001'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90613002','161040') where GLACCOUNT like '%-C90613002'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90613003','161040') where GLACCOUNT like '%-C90613003'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90613004','161040') where GLACCOUNT like '%-C90613004'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C90701000','161040') where GLACCOUNT like '%-C90701000'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'CG80010004','161060') where GLACCOUNT like '%-CG80010004'; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'CG8001005','161060') where GLACCOUNT like '%-CG8001005'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'CG8001006','161030') where GLACCOUNT like '%-CG8001006'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'CG8001007','161060') where GLACCOUNT like '%-CG8001007'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'CG8001008','161060') where GLACCOUNT like '%-CG8001008'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14030','161030') where GLACCOUNT like '%-C6MF14030'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14068','161030') where GLACCOUNT like '%-C6MF14068'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C6MF14090','161030') where GLACCOUNT like '%-C6MF14090'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C82060501','161040') where GLACCOUNT like '%-C82060501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86210501','161030') where GLACCOUNT like '%-C86210501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86210504','161030') where GLACCOUNT like '%-C86210504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900501','161030') where GLACCOUNT like '%-C86900501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86900502','161030') where GLACCOUNT like '%-C86900502'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C8690052','161030') where GLACCOUNT like '%-C8690052'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87510504','161040') where GLACCOUNT like '%-C87510504'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87520503','161040') where GLACCOUNT like '%-C87520503'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87530507','161040') where GLACCOUNT like '%-C87530507'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560503','161040') where GLACCOUNT like '%-C87560503'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560510','161040') where GLACCOUNT like '%-C87560510'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C87560511','161040') where GLACCOUNT like '%-C87560511'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020501','161042') where GLACCOUNT like '%-C88020501'; --1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020503 ','161042') where GLACCOUNT like '%-C88020503 '; -----
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020509','161042') where GLACCOUNT like '%-C88020509'; --1 rows updated.
commit;
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020402'; --1 rows 
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020501'; --1 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'C86900403','161030') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-C86900403'; --4 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'C87510504','161040') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-C87510504'; --29
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'C87560510','161040') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-C87560510'; --1 rows 
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'C88020509','161042') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-C88020509'; --387 
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14000','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14000'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14001','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14001'; --16 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14002'; --36 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14003','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14003'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14004','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14004'; --11 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14005'; --17 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14006','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14006'; --12 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14007','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14007'; --20
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14008','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14008'; --10
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14009','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14009'; --18 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14041'; --19 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14042','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14042'; --7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14044','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14044'; --6 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14052','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14052'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14060'; --14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14061','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14061'; --3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14064'; --27 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14065','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14065'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14071','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14071'; --35 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14072','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14072'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14082','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14082'; --10
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14101','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14101'; --3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14110','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14110'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C75012202','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C75012202'; --2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C77501301','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C77501301'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86620301'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700403','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700403'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700404','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700404'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700406'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700409','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700409'; --18 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700411','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700411'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900301','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900301'; --5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900302','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900302'; --5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900303'; --30
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900401'; --16 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900402'; --14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900403'; --114 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900404','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900404'; --14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900407','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900407'; --4 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900408'; --12 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900409'; --2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86920401','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86920401'; --10
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510401'; --22 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510402'; --34 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510405'; --103 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510407','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510407'; --7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87515402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87515402'; --5 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87515403','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87515403'; --2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520304'; --28 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87520401','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520401'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520402'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87520407','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520407'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530301','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530301'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530402'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530403'; --27 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530405'; --21 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530504','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530504'; --2 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560303','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560303'; --11 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560402'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560404'; --20
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020301'; --27 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020302'; --14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020401'; --41 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020402'; --260
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C82060501','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C82060501'; --3 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900501','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900501'; --7 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900502'; --48 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510504','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510504'; --11 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530507','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530507'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560503','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560503'; --11 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560510','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560510'; --1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560511','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560511'; --14 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020501'; --23 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020509'; --45 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14000','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14000'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14001','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14001'; --16 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14002'; --36 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14003','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14003'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14004','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14004'; --11 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14005','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14005'; --17 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14006','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14006'; --12 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14007','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14007'; --20
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14008','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14008'; --10
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14009','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14009'; --18 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14041','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14041'; --19 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14042','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14042'; --7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14044','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14044'; --6 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14052','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14052'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14060'; --14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14061','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14061'; --3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14064','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14064'; --27 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14065','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14065'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14071','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14071'; --35 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14072','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14072'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14082','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14082'; --10
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14101','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14101'; --3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14110','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14110'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75012202','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C75012202'; --2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C77501301','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C77501301'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86620301'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700403','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700403'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700404','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700404'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700406'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700409','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700409'; --18 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700411','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86700411'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900301','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900301'; --5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900302','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900302'; --5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900303','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900303'; --30
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900401'; --16 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900402','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900402'; --14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900403'; --114 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900404','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900404'; --14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900407','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900407'; --4 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900408'; --12 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900409','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900409'; --2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86920401','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86920401'; --10
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510401'; --22 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510402'; --34 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510405'; --103 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510407','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510407'; --7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87515402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87515402'; --5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87515403','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87515403'; --2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87520304','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520304'; --28 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87520401','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520401'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87520402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520402'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87520407','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87520407'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530301','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530301'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530402'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530403'; --27 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530405'; --21 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530504','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530504'; --2 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560303','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560303'; --11 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560402','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560402'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560404'; --20
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020301'; --27 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020302','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020302'; --14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020401'; --41 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020402'; --260
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C82060501','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C82060501'; --3 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900501','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900501'; --7 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900502','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86900502'; --48 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510504','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87510504'; --11 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530507','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87530507'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560503','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560503'; --11 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560510','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560510'; --1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560511','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C87560511'; --14 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020501','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020501'; --23 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020509'; --45 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C6MF14002'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C6MF14060'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86210302','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86210302'; --3 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86620301'; --26 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700404','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86700404'; --8 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86700406'; --10
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86900401'; --13 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86900403'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87510401'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87510402'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87510405'; --2 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87520405','161060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87520405'; --20
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87530403'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87530405'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87560404'; --1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020301'; --2 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020401'; --7 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020402'; --9 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'CG8001007','161060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-CG8001007'; --2 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C87560510','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C87560510'; --4 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C88020509'; --13 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14002','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14002'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C6MF14060','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C6MF14060'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86210302','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86210302'; --3 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86620301','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86620301'; --26 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700404','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86700404'; --8 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86700406','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86700406'; --10
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900401','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86900401'; --13 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900403','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86900403'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510401','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87510401'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510402','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87510402'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87510405','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87510405'; --2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87520405','161060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87520405'; --20
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530403','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87530403'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87530405','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87530405'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560404','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87560404'; --1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C88020301'; --2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C88020401'; --7 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C88020402'; --9 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'CG8001007','161060') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-CG8001007'; --2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C87560510','161040') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C87560510'; --4 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C88020509'; --13 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C72019014','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C72019014'; --2 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75010201','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75010201'; --12 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75010203','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75010203'; --17 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75011201','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75011201'; --27 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75012202','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75012202'; --2 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75019202','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75019202'; --8 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75100307','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75100307'; --5 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75100312','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75100312'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75150402','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75150402'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75209507','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75209507'; --3 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75300601','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75300601'; --3 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C75309601','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C75309601'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C77017804','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C77017804'; --7 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86900408','161030') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C86900408'; --12 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020301','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C88020301'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020401','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C88020401'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020402','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C88020402'; --6 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C90312000','161040') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C90312000'; --1 rows updated.
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020509','161042') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-C88020509'; --13 rows updated.
commit;
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C22060302','151300') where historyflag=0 and GLACCOUNT like '%-C22060302'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C23010401','151300') where historyflag=0 and GLACCOUNT like '%-C23010401'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6ENG1001','161030') where historyflag=0 and GLACCOUNT like '%-C6ENG1001'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14000','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14000'; --8 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14001','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14001'; --23 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14002','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14002'; --63 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14003','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14003'; --7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14004','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14004'; --14 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14005','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14005'; --23 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14006','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14006'; --19 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14007','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14007'; --27 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14008','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14008'; --12 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14009','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14009'; --23 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14020','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14020'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14021','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14021'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14022','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14022'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14040','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14040'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14041','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14041'; --55 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14042','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14042'; --20
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14043','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14043'; --3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14044','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14044'; --15 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14050','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14050'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14052','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14052'; --3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14060','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14060'; --12 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14061','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14061'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14062','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14062'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14063','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14063'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14064','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14064'; --34 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14065','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14065'; --5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14066','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14066'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14067','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14067'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14071','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14071'; --65 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14072','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14072'; --7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14082','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14082'; --13 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14101','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14101'; --6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14110','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14110'; --8 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MIN0809','161030') where historyflag=0 and GLACCOUNT like '%-C6MIN0809'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MNT1112','161030') where historyflag=0 and GLACCOUNT like '%-C6MNT1112'; --3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MNT1205','161030') where historyflag=0 and GLACCOUNT like '%-C6MNT1205'; --6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6OPS1201','161030') where historyflag=0 and GLACCOUNT like '%-C6OPS1201'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6W654115','161032') where historyflag=0 and GLACCOUNT like '%-C6W654115'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C75012202','161042') where historyflag=0 and GLACCOUNT like '%-C75012202'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C75202501','161040') where historyflag=0 and GLACCOUNT like '%-C75202501'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C77501301','161040') where historyflag=0 and GLACCOUNT like '%-C77501301'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86610303','161030') where historyflag=0 and GLACCOUNT like '%-C86610303'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86610306','161030') where historyflag=0 and GLACCOUNT like '%-C86610306'; --3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86610307','161030') where historyflag=0 and GLACCOUNT like '%-C86610307'; --3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86620301','161030') where historyflag=0 and GLACCOUNT like '%-C86620301'; --55 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700402','161030') where historyflag=0 and GLACCOUNT like '%-C86700402'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700403','161030') where historyflag=0 and GLACCOUNT like '%-C86700403'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700404','161030') where historyflag=0 and GLACCOUNT like '%-C86700404'; --14 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700405','161030') where historyflag=0 and GLACCOUNT like '%-C86700405'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700406','161030') where historyflag=0 and GLACCOUNT like '%-C86700406'; --9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700409','161030') where historyflag=0 and GLACCOUNT like '%-C86700409'; --27 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700410','161030') where historyflag=0 and GLACCOUNT like '%-C86700410'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86700411','161030') where historyflag=0 and GLACCOUNT like '%-C86700411'; --9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900301','161030') where historyflag=0 and GLACCOUNT like '%-C86900301'; --23 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900302','161030') where historyflag=0 and GLACCOUNT like '%-C86900302'; --21 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900303','161030') where historyflag=0 and GLACCOUNT like '%-C86900303'; --73 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900401','161030') where historyflag=0 and GLACCOUNT like '%-C86900401'; --49 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900402','161030') where historyflag=0 and GLACCOUNT like '%-C86900402'; --34 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900403','161030') where historyflag=0 and GLACCOUNT like '%-C86900403'; --153 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900404','161030') where historyflag=0 and GLACCOUNT like '%-C86900404'; --17 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900407','161030') where historyflag=0 and GLACCOUNT like '%-C86900407'; --5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900408','161030') where historyflag=0 and GLACCOUNT like '%-C86900408'; --26 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900409','161030') where historyflag=0 and GLACCOUNT like '%-C86900409'; --6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86920401','161030') where historyflag=0 and GLACCOUNT like '%-C86920401'; --6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87510401','161040') where historyflag=0 and GLACCOUNT like '%-C87510401'; --7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87510402','161040') where historyflag=0 and GLACCOUNT like '%-C87510402'; --17 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87510405','161040') where historyflag=0 and GLACCOUNT like '%-C87510405'; --20
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87510407','161040') where historyflag=0 and GLACCOUNT like '%-C87510407'; --10
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87515402','161040') where historyflag=0 and GLACCOUNT like '%-C87515402'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87515403','161040') where historyflag=0 and GLACCOUNT like '%-C87515403'; --5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520304','161040') where historyflag=0 and GLACCOUNT like '%-C87520304'; --112 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520401','161040') where historyflag=0 and GLACCOUNT like '%-C87520401'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520402','161040') where historyflag=0 and GLACCOUNT like '%-C87520402'; --6 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520404','161060') where historyflag=0 and GLACCOUNT like '%-C87520404'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520407','161040') where historyflag=0 and GLACCOUNT like '%-C87520407'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530301','161040') where historyflag=0 and GLACCOUNT like '%-C87530301'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530402','161040') where historyflag=0 and GLACCOUNT like '%-C87530402'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530403','161040') where historyflag=0 and GLACCOUNT like '%-C87530403'; --9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530405','161040') where historyflag=0 and GLACCOUNT like '%-C87530405'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530504','161040') where historyflag=0 and GLACCOUNT like '%-C87530504'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560301','161040') where historyflag=0 and GLACCOUNT like '%-C87560301'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560303','161040') where historyflag=0 and GLACCOUNT like '%-C87560303'; --49 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560401','161040') where historyflag=0 and GLACCOUNT like '%-C87560401'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560402','161040') where historyflag=0 and GLACCOUNT like '%-C87560402'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560404','161040') where historyflag=0 and GLACCOUNT like '%-C87560404'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020301','161042') where historyflag=0 and GLACCOUNT like '%-C88020301'; --26 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020302','161040') where historyflag=0 and GLACCOUNT like '%-C88020302'; --31 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020401','161042') where historyflag=0 and GLACCOUNT like '%-C88020401'; --37 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020402','161042') where historyflag=0 and GLACCOUNT like '%-C88020402'; --225 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'CG8001005','161060') where historyflag=0 and GLACCOUNT like '%-CG8001005'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'CG8001007','161060') where historyflag=0 and GLACCOUNT like '%-CG8001007'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'CG8001008','161060') where historyflag=0 and GLACCOUNT like '%-CG8001008'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C6MF14090','161030') where historyflag=0 and GLACCOUNT like '%-C6MF14090'; --5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C82060501','161040') where historyflag=0 and GLACCOUNT like '%-C82060501'; --4 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86210501','161030') where historyflag=0 and GLACCOUNT like '%-C86210501'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86210504','161030') where historyflag=0 and GLACCOUNT like '%-C86210504'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900501','161030') where historyflag=0 and GLACCOUNT like '%-C86900501'; --10
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86900502','161030') where historyflag=0 and GLACCOUNT like '%-C86900502'; --29 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87510504','161040') where historyflag=0 and GLACCOUNT like '%-C87510504'; --9 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87520503','161040') where historyflag=0 and GLACCOUNT like '%-C87520503'; --1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87530507','161040') where historyflag=0 and GLACCOUNT like '%-C87530507'; --2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560503','161040') where historyflag=0 and GLACCOUNT like '%-C87560503'; --8 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560510','161040') where historyflag=0 and GLACCOUNT like '%-C87560510'; --7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C87560511','161040') where historyflag=0 and GLACCOUNT like '%-C87560511'; --7 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020501','161042') where historyflag=0 and GLACCOUNT like '%-C88020501'; --40
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020509','161042') where historyflag=0 and GLACCOUNT like '%-C88020509'; --320
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C22060302','151300') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C22060302'; --5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C23010401','151300') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C23010401'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6ENG1001','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6ENG1001'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14000','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14000'; --53 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14001','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14001'; --141 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14002','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14002'; --440
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14003','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14003'; --43 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14004','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14004'; --92 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14005','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14005'; --157 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14006','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14006'; --119 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14007','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14007'; --186 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14008','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14008'; --77 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14009','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14009'; --170
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14021','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14021'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14022','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14022'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14040','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14040'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14041','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14041'; --390
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14042','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14042'; --129 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14043','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14043'; --12 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14044','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14044'; --109 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14050','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14050'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14052','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14052'; --21 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14054','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14054'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14060','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14060'; --68 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14061','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14061'; --24 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14062','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14062'; --8 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14064','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14064'; --221 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14065','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14065'; --14 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14071','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14071'; --433 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14072','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14072'; --40
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14082','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14082'; --82 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14101','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14101'; --28 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14110','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14110'; --34 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MIN0809','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MIN0809'; --12 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MNT1112','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MNT1112'; --5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MNT1205','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MNT1205'; --6 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6OPS1201','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6OPS1201'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6W654115','161032') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6W654115'; --5 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C75012202','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C75012202'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C75202501','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C75202501'; --3 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C77501301','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C77501301'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86610303','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86610303'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86610306','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86610306'; --6 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86610307','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86610307'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86620301','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86620301'; --177 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700402','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700402'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700403','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700403'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700404','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700404'; --60
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700405','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700405'; --7 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700406','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700406'; --42 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700409','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700409'; --124 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700410','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700410'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86700411','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86700411'; --62 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900301','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900301'; --94 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900302','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900302'; --79 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900303','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900303'; --276 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900401','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900401'; --140
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900402','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900402'; --105 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900403','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900403'; --718 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900404','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900404'; --93 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900407','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900407'; --23 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900408','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900408'; --151 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900409','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900409'; --28 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86920301','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86920301'; --2 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86920304','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86920304'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86920401','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86920401'; --26 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510305','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510305'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510401','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510401'; --55 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510402','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510402'; --139 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510405','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510405'; --152 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510407','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510407'; --63 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87515402','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87515402'; --13 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87515403','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87515403'; --25 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520303','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520303'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520304','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520304'; --542 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520401','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520401'; --8 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520402','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520402'; --28 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520404','161060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520404'; --15 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520407','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520407'; --14 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530301','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530301'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530402','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530402'; --8 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530403','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530403'; --72 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530405','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530405'; --31 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530504','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530504'; --14 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560301','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560301'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560303','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560303'; --165 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560401','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560401'; --11 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560402','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560402'; --8 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560404','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560404'; --16 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020301','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020301'; --113 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020302','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020302'; --153 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020401','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020401'; --317 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020402','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020402'; --1,844 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'CG8001005','161060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-CG8001005'; --4 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'CG8001007','161060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-CG8001007'; --10
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'CG8001008','161060') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-CG8001008'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C6MF14090','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C6MF14090'; --31 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C82060501','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C82060501'; --22 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86210501','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86210501'; --1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86210504','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86210504'; --9 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900501','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900501'; --47 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86900502','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86900502'; --159 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87510504','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87510504'; --52 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87520503','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87520503'; --2 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87530507','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87530507'; --8 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560503','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560503'; --61 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560510','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560510'; --29 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C87560511','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C87560511'; --27 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020501','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020501'; --257 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020509','161042') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020509'; --910
commit;
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86620502','161030') where GLACCOUNT like '%-C86620502'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86620503','161030') where GLACCOUNT like '%-C86620503'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920501','161030') where GLACCOUNT like '%-C86920501'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86920502','161030') where GLACCOUNT like '%-C86920502'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020513','161040') where GLACCOUNT like '%-C88020513'; -- 1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86620503','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86620503'; -- 5 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86620503','161030') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C86620503'; -- 5 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86920501','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86920501'; -- 1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86920502','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86920502'; -- 2 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86920501','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86920501'; -- 1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86920502','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86920502'; -- 2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86620502','161030') where historyflag=0 and GLACCOUNT like '%-C86620502'; -- 1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86620503','161030') where historyflag=0 and GLACCOUNT like '%-C86620503'; -- 5 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86920501','161030') where historyflag=0 and GLACCOUNT like '%-C86920501'; -- 2 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86920502','161030') where historyflag=0 and GLACCOUNT like '%-C86920502'; -- 3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020513','161040') where historyflag=0 and GLACCOUNT like '%-C88020513'; -- 3 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86620502','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86620502'; -- 2 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86620503','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86620503'; -- 34 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86920501','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86920501'; -- 3 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86920502','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86920502'; -- 15 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020513','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020513'; -- 6 rows updated.
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86620503','161030') where GLDEBITACCT like '%-C86620503' and transdate > '31/DEC/2014'; -- 2 rows updated.
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'C86620503','161030') where GLDEBITACCT like '%-C86620503' and transdate > '31/DEC/2014'; -- 2 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C86620503','161030') where WOGLACCOUNT like '%-C86620503' and transdate > '31/DEC/2014'; -- 4 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'C88020513','161040') where WOGLACCOUNT like '%-C88020513' and transdate > '31/DEC/2014'; -- 2 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'C86620503','161030') where GLDEBITACCT like '%-C86620503' and transdate > '31/DEC/2014'; -- 4 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86620501','161030') where GLACCOUNT like '%-C86620501'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C86620504','161030') where GLACCOUNT like '%-C86620504'; -- 1 rows updated.
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'C88020512','161040') where GLACCOUNT like '%-C88020512'; -- 1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020512','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020512'; -- 1 rows updated.
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'C88020513','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020513'; -- 1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020512','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020512'; -- 1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'C88020513','161040') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-C88020513'; -- 1 rows updated.
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'C86620501','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-C86620501'; -- 1 rows updated.
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'C86620501','161030') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-C86620501'; -- 1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86620501','161030') where historyflag=0 and GLACCOUNT like '%-C86620501'; -- 3 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C86620504','161030') where historyflag=0 and GLACCOUNT like '%-C86620504'; -- 1 rows updated.
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'C88020512','161040') where historyflag=0 and GLACCOUNT like '%-C88020512'; -- 1 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C86620501','161030') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C86620501'; -- 13 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'C88020512','161040') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-C88020512'; -- 9 rows updated.
--170 min -- 48 min