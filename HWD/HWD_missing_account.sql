update ASSET set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update ASSET set TOOLCONTROLACCOUNT = replace(TOOLCONTROLACCOUNT,'51325','600050') where TOOLCONTROLACCOUNT like '%-51325';
update ASSET set ROTSUSPACCT = replace(ROTSUSPACCT,'51325','600050') where ROTSUSPACCT like '%-51325';
update COMPANIES set APSUSPENSEACC = replace(APSUSPENSEACC,'51325','600050') where APSUSPENSEACC like '%-51325';
update COMPANIES set APCONTROLACC = replace(APCONTROLACC,'51325','600050') where APCONTROLACC like '%-51325';
update COMPANIES set RBNIACC = replace(RBNIACC,'51325','600050') where RBNIACC like '%-51325';
update COMPANIES set TOOLCONTROLACCOUNT = replace(TOOLCONTROLACCOUNT,'51325','600050') where TOOLCONTROLACCOUNT like '%-51325';
update COMPANIES set CONSACCT = replace(CONSACCT,'51325','600050') where CONSACCT like '%-51325';
update FINCNTRL set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update INVCOST set CONTROLACC = replace(CONTROLACC,'51325','600050') where CONTROLACC like '%-51325';
update INVCOST set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update INVCOST set SHRINKAGEACC = replace(SHRINKAGEACC,'51325','600050') where SHRINKAGEACC like '%-51325';
update INVCOST set INVCOSTADJACC = replace(INVCOSTADJACC,'51325','600050') where INVCOSTADJACC like '%-51325';
update INVENTORY set CONTROLACC = replace(CONTROLACC,'51325','600050') where CONTROLACC like '%-51325';
update INVENTORY set INVCOSTADJACC = replace(INVCOSTADJACC,'51325','600050') where INVCOSTADJACC like '%-51325';
update INVENTORY set SHRINKAGEACC = replace(SHRINKAGEACC,'51325','600050') where SHRINKAGEACC like '%-51325';
update INVENTORY set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update INVOICE set APSUSPENSEACCT = replace(APSUSPENSEACCT,'51325','600050') where historyflag=0 AND APSUSPENSEACCT like '%-51325';
update INVOICE set APCONTROLACCT = replace(APCONTROLACCT,'51325','600050') where historyflag=0 AND APCONTROLACCT like '%-51325';
update INVOICE set TAX1GL = replace(TAX1GL,'51325','600050') where historyflag=0 AND TAX1GL like '%-51325';
update INVOICECOST set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLDEBITACCT like '%-51325';
update INVOICECOST set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where invoicenum IN (SELECT invoicenum FROM invoice WHERE historyflag=0) AND  GLCREDITACCT like '%-51325';
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where wonum is not null and wonum in (select wonum from WORKORDER where historyflag=0) AND GLACCOUNT like '%-51325';
update INVRESERVE set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where ponum is not null and ponum in (select ponum from PO where historyflag=0) AND GLACCOUNT like '%-51325';
update INVUSELINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325';
update INVUSELINE set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325';
update ITEMORGINFO set CONTROLACC = replace(CONTROLACC,'51325','600050') where CONTROLACC like '%-51325';
update ITEMORGINFO set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update LABORCRAFTRATE set DEFAULTTICKETGLACC = replace(DEFAULTTICKETGLACC,'51325','600050') where DEFAULTTICKETGLACC like '%-51325';
update LABORCRAFTRATE set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update LABORCRAFTRATE set CONTROLACCOUNT = replace(CONTROLACCOUNT,'51325','600050') where CONTROLACCOUNT like '%-51325';
update LOCATIONS set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';--2 rows updated.
update LOCATIONS set TOOLCONTROLACC = replace(TOOLCONTROLACC,'51325','600050') where TOOLCONTROLACC like '%-51325';
update LOCATIONS set OLDCONTROLACC = replace(OLDCONTROLACC,'51325','600050') where OLDCONTROLACC like '%-51325';
update LOCATIONS set PURCHVARACC = replace(PURCHVARACC,'51325','600050') where PURCHVARACC like '%-51325';
update LOCATIONS set CONTROLACC = replace(CONTROLACC,'51325','600050') where CONTROLACC like '%-51325';
update LOCATIONS set INTLABREC = replace(INTLABREC,'51325','600050') where INTLABREC like '%-51325';
update LOCATIONS set CURVARACC = replace(CURVARACC,'51325','600050') where CURVARACC like '%-51325';
update LOCATIONS set SHRINKAGEACC = replace(SHRINKAGEACC,'51325','600050') where SHRINKAGEACC like '%-51325';
update LOCATIONS set RECEIPTVARACC = replace(RECEIPTVARACC,'51325','600050') where RECEIPTVARACC like '%-51325';
update LOCATIONS set INVOICEVARACC = replace(INVOICEVARACC,'51325','600050') where INVOICEVARACC like '%-51325';
update LOCATIONS set OLDSHRINKAGEACC = replace(OLDSHRINKAGEACC,'51325','600050') where OLDSHRINKAGEACC like '%-51325';
update LOCATIONS set INVCOSTADJACC = replace(INVCOSTADJACC,'51325','600050') where INVCOSTADJACC like '%-51325';
update LOCATIONS set OLDINVCOSTADJACC = replace(OLDINVCOSTADJACC,'51325','600050') where OLDINVCOSTADJACC like '%-51325';
update PM set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update POCOST set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-51325';
update POCOST set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--1 rows updated.
update POLINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--1 rows updated.
update POLINE set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where ponum IN (SELECT ponum FROM po WHERE historyflag=0) AND GLCREDITACCT like '%-51325';
update PRCOST set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLDEBITACCT like '%-51325';--2 rows updated.
update PRCOST set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND  GLCREDITACCT like '%-51325';
update PRLINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLDEBITACCT like '%-51325';--2 rows updated.
update PRLINE set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where prnum IN (SELECT prnum FROM pr WHERE historyflag=0) AND GLCREDITACCT like '%-51325';
update QUOTATIONLINE set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLCREDITACCT like '%-51325';
update REORDERPAD set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update REORDERPAD set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325';
update REORDERPAD set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325';
update RFQLINE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLDEBITACCT like '%-51325';
update RFQVENDOR set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where rfqnum IN (SELECT rfqnum FROM rfq WHERE historyflag=0) AND GLCREDITACCT like '%-51325';
update WORKORDER set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where historyflag=0 and GLACCOUNT like '%-51325';--12 rows updated.
update WOSTATUS set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where wonum IN (SELECT wonum FROM workorder WHERE historyflag=0) AND GLACCOUNT like '%-51325';--84 rows updated.
update CHARTOFACCOUNTS set GLACCOUNT = replace(GLACCOUNT,'51325','600050') where GLACCOUNT like '%-51325';
update CHARTOFACCOUNTS set GLCOMP03 = replace(GLCOMP03,'51325','600050') where GLCOMP03 = '51325';
update ASSETTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update ASSETTRANS set TLOAMFROMGLACCOUNT = replace(TLOAMFROMGLACCOUNT,'51325','600050') where TLOAMFROMGLACCOUNT like '%-51325' and transdate > '31/DEC/2014';
update ASSETTRANS set TLOAMTOGLACCOUNT = replace(TLOAMTOGLACCOUNT,'51325','600050') where TLOAMTOGLACCOUNT like '%-51325' and transdate > '31/DEC/2014';
update ASSETTRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update INVOICETRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update INVOICETRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update INVTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update INVTRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update LABTRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update LABTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update MATRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update MATRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update MATUSETRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update MATUSETRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';
update SERVRECTRANS set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
update SERVRECTRANS set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';--1 rows updated.
update SUNGLTXN_IFACE set WOGLACCOUNT = replace(WOGLACCOUNT,'51325','600050') where WOGLACCOUNT like '%-51325' and transdate > '31/DEC/2014';--6 rows updated.
update SUNGLTXN_IFACE set GLDEBITACCT = replace(GLDEBITACCT,'51325','600050') where GLDEBITACCT like '%-51325' and transdate > '31/DEC/2014';--2 rows updated.
update SUNGLTXN_IFACE set GLCREDITACCT = replace(GLCREDITACCT,'51325','600050') where GLCREDITACCT like '%-51325' and transdate > '31/DEC/2014';
